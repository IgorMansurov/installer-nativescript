import { Component, ViewChild, ElementRef } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/directives/dialogs";
import {View} from "ui/core/view";

@Component({
    moduleId: module.id,
    selector: "installer-code",
    templateUrl: "./installer-code.html",
    styleUrls: ["./installer-code-common.css", "./installer-code.css"],
})
export class InstallerCodeComponent {
    public installerCode: number;

    @ViewChild('codeField') codeField: ElementRef;

    public constructor(private params: ModalDialogParams) {
        this.installerCode = null;
    }
    ngAfterViewInit() {
        let codeField = <View>this.codeField.nativeElement;
        setTimeout(function () { codeField.focus() }, 300);
    }
    public submit() {
        this.params.closeCallback(this.installerCode);
    }
}