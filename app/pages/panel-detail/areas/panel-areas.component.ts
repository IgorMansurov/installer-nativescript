import { Component, OnInit } from "@angular/core";
import { RouterService } from "../../../shared/router.service";
import { PageRoute } from "nativescript-angular/router";
import { PanelAreasService } from "../../../shared/panel/panel-areas.service";

@Component({
    moduleId: module.id,
    selector: "panel-areas",
    templateUrl: "./panel-areas.html",
    styleUrls: ["./panel-areas-common.css", "./panel-areas.css"],
    providers: []
})
export class PanelAreasComponent implements OnInit {

    public areas: any;
    public panelId: number;
    public title: string;

    constructor(private panelAreasService: PanelAreasService, private pageRoute: PageRoute, public routerService: RouterService) {
        this.routerService.isLoading = true;
        this.pageRoute.activatedRoute
            .switchMap(activatedRoute => activatedRoute.params)
            .forEach((params) => {
                this.panelId = +params["panelId"];
                this.title = "Panel " + this.panelId + ' > Areas';
            });
    }

    ngOnInit() {
        this.areas = this.panelAreasService.getPanelAreaList(this.panelId);
    }

    public editArea(areaId: number) {
        this.routerService.isLoading = true;
        this.routerService.simpleForward('/panel-detail/' + this.panelId + '/areas/' + areaId);
    }

    public onNavBackBtnTap() {
        this.routerService.isLoading = true;
        this.routerService.simpleBackward('/panel-detail/' + this.panelId);
    }

    public increaseIndex(index: any) {
        return String(Number(index) + 1);
    }
}