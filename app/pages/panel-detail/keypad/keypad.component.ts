import { Component, OnInit, ViewChild } from "@angular/core";
import { PageRoute } from "nativescript-angular/router";
import { KeypadService } from "./keypad.service";
import { Helper } from "./../../../shared/helper";

import { ButtonEditorHelper } from "../../../shared/button-helper";
import { RadDataFormComponent } from "nativescript-pro-ui/dataform/angular";
import { android as androidApplication } from "tns-core-modules/application";

import { BarcodeScanService } from "../../../shared/barcode-scan.service";
import { RouterService } from "../../../shared/router.service";


class KeypadMain {
    public keypads_list: number;
}

@Component({
    moduleId: module.id,
    selector: "keypad",
    templateUrl: "./keypad.html",
    styleUrls: ["./keypad.css", "./keypad.css"],
    providers: []
})

export class KeypadComponent implements OnInit {

    public title: string;
    private configKeypad: any;
    private configKeypadBackup: any;
    public panelId: number;
    public keypadId: number;

    public keypadMainProperties: any;
    public settings: any;
    public keypad_assigned_to_area: any;
    public user_can_operate_at_keypad: any;

    public keypad_medical_alarm_to_output: any;
    public keypad_panic_alarm_to_output: any;
    public keypad_fire_alarm_to_output: any;

    public labelTextSize: number;

    public buttonBarcode: any;
    private _buttonEditorHelper;

    public keys(object: any) {
        return Helper.keys(object);
    }


    constructor(private pageRoute: PageRoute, private keypadService: KeypadService, public routerService: RouterService, private barcodeScanService: BarcodeScanService) {
        this.pageRoute.activatedRoute
            .switchMap(activatedRoute => activatedRoute.params)
            .forEach((params) => {
                this.panelId = +params["panelId"];
                this.keypadId = +params["keypadId"];
                this.title = "Panel " + this.panelId + ' > Keypad > ' + this.keypadId;
            });
    }

    ngOnInit() {
        this.configKeypad = this.keypadService.getKeypad(this.panelId, this.keypadId);
        if (this.configKeypad) {
            this.configKeypadBackup = this.configKeypad;
            this._deComposeConfigOutput();
        }
        this.labelTextSize = 16;
    }

    @ViewChild('keypadMainPropertiesForm') keypadMainPropertiesForm: RadDataFormComponent;

    private _deComposeConfigOutput() {

        this.settings = {};
        this.settings.beeps_enabled = this.configKeypad.beeps_enabled;
        this.settings.no_keypad_indications_when_armed = this.configKeypad.no_keypad_indications_when_armed;

        this.keypad_assigned_to_area = {};

        for (let areaId in this.configKeypad.keypad_assigned_to_area) {
            this.keypad_assigned_to_area[areaId] = this.configKeypad.keypad_assigned_to_area[areaId]["assigned"];
        }

        this.user_can_operate_at_keypad = {};

        for (let userId in this.configKeypad.user_can_operate_at_keypad) {
            this.user_can_operate_at_keypad[userId] = this.configKeypad.user_can_operate_at_keypad[userId]["assigned"];
        }

        this.keypad_medical_alarm_to_output = {};
        this.keypad_panic_alarm_to_output = {};
        this.keypad_fire_alarm_to_output = {};

        for (let outputId in this.configKeypad.keypad_medical_alarm_to_output) {
            this.keypad_medical_alarm_to_output[outputId] = this.configKeypad.keypad_medical_alarm_to_output[outputId]["assigned"];
            this.keypad_panic_alarm_to_output[outputId] = this.configKeypad.keypad_panic_alarm_to_output[outputId]["assigned"];
            this.keypad_fire_alarm_to_output[outputId] = this.configKeypad.keypad_fire_alarm_to_output[outputId]["assigned"];
        }

        this.keypadMainProperties = new KeypadMain();
        this.keypadMainProperties.keypads_list = this.configKeypad.keypads_list;
        this.keypadMainProperties.buttonBarcode = this.keypadMainProperties.keypads_list;
    }

    private _composeConfigOutput() {

        this.configKeypad.beeps_enabled = this.settings.beeps_enabled;
        this.configKeypad.no_keypad_indications_when_armed = this.settings.no_keypad_indications_when_armed;

        for (let areaId in this.configKeypad.keypad_assigned_to_area) {
            this.configKeypad.keypad_assigned_to_area[areaId]["assigned"] = this.keypad_assigned_to_area[areaId];
        }

        for (let userId in this.configKeypad.user_can_operate_at_keypad) {
            this.configKeypad.user_can_operate_at_keypad[userId]["assigned"] = this.user_can_operate_at_keypad[userId];
        }

        for (let outputId in this.configKeypad.keypad_medical_alarm_to_output) {
            this.configKeypad.keypad_medical_alarm_to_output[outputId]["assigned"] = this.keypad_medical_alarm_to_output[outputId];
            this.configKeypad.keypad_panic_alarm_to_output[outputId]["assigned"] = this.keypad_panic_alarm_to_output[outputId];
            this.configKeypad.keypad_fire_alarm_to_output[outputId]["assigned"] = this.keypad_fire_alarm_to_output[outputId];
        }

        this.configKeypad.keypads_list = this.keypadMainProperties.keypads_list;

    }

    public save() {
        this.keypadMainPropertiesForm.dataForm.commitAll();
        this.routerService.isLoading = true;
        this._composeConfigOutput();
        this.keypadService.saveKeypad(this.panelId, this.configKeypad)
            .then(res => {
                this.routerService.simpleBackward('/panel-detail/' + this.panelId + '/keypads/');
            })
            .catch(error => {
                this.configKeypad = this.configKeypadBackup;
                this.keypadService.restoreKeypad(this.panelId, this.configKeypadBackup);
                alert(error);
                this.routerService.isLoading = false;
            });
    }

    public onNavBackBtnTap() {
        this.routerService.isLoading = true;
        this.routerService.simpleBackward('/panel-detail/' + this.panelId + '/keypads/');
    }

    public editorNeedsView(args) {
        if (androidApplication) {
            this._buttonEditorHelper = new ButtonEditorHelper();
            this._buttonEditorHelper.editor = args.object;
            let androidEditorView: android.widget.Button = new android.widget.Button(args.context);
            let that = this;
            androidEditorView.setOnClickListener(new android.view.View.OnClickListener({
                onClick(view: android.view.View) {
                    that.handleTap(view, args.object);
                }
            }));
            args.view = androidEditorView;
            this.updateEditorValue(androidEditorView, this.keypadMainProperties.keypads_list);
        } else {
            this._buttonEditorHelper = new ButtonEditorHelper();
            this._buttonEditorHelper.editor = args.object;
            let iosEditorView = UIButton.buttonWithType(UIButtonType.System);
            iosEditorView.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left;
            iosEditorView.addTargetActionForControlEvents(this._buttonEditorHelper, "handleTap:", UIControlEvents.TouchUpInside);
            args.view = iosEditorView;
        }
    }

    public editorHasToApplyValue(args) {
        this._buttonEditorHelper.updateEditorValue(args.view, args.value);
    }

    public editorNeedsValue(args) {
        args.value = this._buttonEditorHelper.buttonValue;
    }

    public updateEditorValue(editorView, value) {
        this._buttonEditorHelper.buttonValue = value;
        editorView.setText("Tap to scan pendant barcode");
    }

    public handleTap(editorView, editor) {
        this.barcodeScanService.scanBarcode()
            .then((result) => {
                    this.updateEditorValue(editorView, result.text);
                    editor.notifyValueChanged();
                    /* TODO
                    https://github.com/telerik/nativescript-ui-feedback/issues/344
                    * */
                    Helper.reloadRadDataFormProperties(this, 'keypadMainPropertiesForm', 'keypadMainProperties', {
                        "keypads_list": +result.text
                    });
                }, (errorMessage) => {
                    alert("No scan. " + errorMessage);
                    console.log("No scan. " + errorMessage);
                }
            )
            .catch((error) => alert('Error scan barcode.. ' + error));
    }
}