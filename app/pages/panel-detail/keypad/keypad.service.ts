import { Injectable } from "@angular/core";
import { PanelConfigService} from "../../../shared/panel/panel-config.service";
import { Helper } from "../../../shared/helper";

@Injectable()
export class KeypadService {

    constructor(private panelConfigService: PanelConfigService) {}

    public saveKeypad(panelId: number, keypad: any) {
        this.saveKeypadLocally(panelId, keypad);
        return this.panelConfigService.saveConfig();
    }
    public restoreKeypad(panelId: number, output: any) {
        this.saveKeypadLocally(panelId, output);
    }
    public saveKeypadLocally(panelId: number, keypad: any) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        let keysToIgnore = ['id', 'user_can_operate_at_keypad', 'keypad_assigned_to_area', 'keypad_medical_alarm_to_output', 'keypad_panic_alarm_to_output', 'keypad_fire_alarm_to_output'];
        for (let key in keypad) {
            if(keysToIgnore.indexOf(key) === -1) {
                panelConfig[key][keypad.id] = keypad[key];
            }
        }
        for (let userId in keypad.user_can_operate_at_keypad) {
            panelConfig.user_can_operate_at_keypad[keypad.id][userId] = keypad.user_can_operate_at_keypad[userId]['assigned'];
        }
        for (let areaId in keypad.keypad_assigned_to_area) {
            panelConfig.keypad_assigned_to_area[areaId][keypad.id] = keypad.keypad_assigned_to_area[areaId]['assigned'];
        }
        for (let outputId in keypad.keypad_medical_alarm_to_output) {
            panelConfig.keypad_medical_alarm_to_output[outputId][keypad.id] = keypad.keypad_medical_alarm_to_output[outputId]['assigned'];
        }
        for (let outputId in keypad.keypad_panic_alarm_to_output) {
            panelConfig.keypad_panic_alarm_to_output[outputId][keypad.id] = keypad.keypad_panic_alarm_to_output[outputId]['assigned'];
        }
        for (let outputId in keypad.keypad_fire_alarm_to_output) {
            panelConfig.keypad_fire_alarm_to_output[outputId][keypad.id] = keypad.keypad_fire_alarm_to_output[outputId]['assigned'];
        }

        this.panelConfigService.saveConfigLocally(panelConfig);
    }
    public getKeypad(panelId: number, keypadId: number) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        if(panelConfig) {
            let keypad_assigned_to_area = {};
            let user_can_operate_at_keypad = {};
            let keypad_medical_alarm_to_output = {};
            let keypad_panic_alarm_to_output = {};
            let keypad_fire_alarm_to_output = {};

            panelConfig.user_code.map((element, userId, array) => {
                if (!Helper.isEmpty(element)) {
                    user_can_operate_at_keypad[userId] = {
                        name: panelConfig.user_name[userId],
                        assigned: panelConfig.user_can_operate_at_keypad[keypadId][userId]
                    };
                }
            });

            for(let areaId in panelConfig.keypad_assigned_to_area) {
                keypad_assigned_to_area[areaId] = {
                    name: panelConfig.area_name[areaId],
                    assigned: panelConfig.keypad_assigned_to_area[areaId][keypadId]
                };
            }

            panelConfig.radio_outputs_list.map((element, outputId, array) => {
                if (!Helper.isEmpty(element)) {
                    keypad_medical_alarm_to_output[outputId] = {
                        name: panelConfig.output_name[outputId],
                        assigned: panelConfig.keypad_medical_alarm_to_output[outputId][keypadId]
                    };
                    keypad_panic_alarm_to_output[outputId] = {
                        name: panelConfig.output_name[outputId],
                        assigned: panelConfig.keypad_panic_alarm_to_output[outputId][keypadId]
                    };
                    keypad_fire_alarm_to_output[outputId] = {
                        name: panelConfig.output_name[outputId],
                        assigned: panelConfig.keypad_fire_alarm_to_output[outputId][keypadId]
                    };
                }
            });

            return {
                id: keypadId,
                keypads_list: panelConfig.keypads_list[keypadId],
                beeps_enabled: panelConfig.beeps_enabled[keypadId],
                no_keypad_indications_when_armed: panelConfig.no_keypad_indications_when_armed[keypadId],
                keypad_assigned_to_area: keypad_assigned_to_area,
                user_can_operate_at_keypad: user_can_operate_at_keypad,
                keypad_medical_alarm_to_output: keypad_medical_alarm_to_output,
                keypad_panic_alarm_to_output: keypad_panic_alarm_to_output,
                keypad_fire_alarm_to_output: keypad_fire_alarm_to_output
            }
        }
        else {
            return {};
        }

    }
}