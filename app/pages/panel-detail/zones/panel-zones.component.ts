import { Component, OnInit } from "@angular/core";
import { RouterService } from "../../../shared/router.service";
import { PageRoute } from "nativescript-angular/router";
import { PanelZonesService } from "../../../shared/panel/panel-zones.service";

@Component({
    moduleId: module.id,
    selector: "panel-zones",
    templateUrl: "./panel-zones.html",
    styleUrls: ["./panel-zones-common.css", "./panel-zones.css"],
    providers: []
})
export class PanelZonesComponent implements OnInit {

    public zones: any;
    public panelId: number;
    public title: string;

    /*
      TODO: https://github.com/bradmartin/nativescript-floatingactionbutton/issues/61
    */

    constructor(private panelZonesService: PanelZonesService, private pageRoute: PageRoute, public routerService: RouterService) {
        this.routerService.isLoading = true;
        this.pageRoute.activatedRoute
            .switchMap(activatedRoute => activatedRoute.params)
            .forEach((params) => {
                this.panelId = +params["panelId"];
                this.title = "Panel " + this.panelId + ' > Zones';
            });
    }

    ngOnInit() {
        this.zones = this.panelZonesService.getZoneList(this.panelId);
    }

    public showAddZoneButton() {
        return this.panelZonesService.quantityEmptyZones(this.panelId) > 0;
    }

    public editZone(zoneId: number = -1) {
        this.routerService.isLoading = true;
        if(zoneId === -1) {
          zoneId = this.panelZonesService.getFirstEmptyZone(this.panelId);
        }
        this.routerService.simpleForward('/panel-detail/' + this.panelId + '/zones/' + zoneId);
    }

    public onNavBackBtnTap() {
        this.routerService.isLoading = true;
        this.routerService.simpleBackward('/panel-detail/' + this.panelId);
    }
}