import { Component, OnInit, ViewChild} from "@angular/core";
import { PageRoute } from "nativescript-angular/router";
import { PanelOutputService } from "./panel-output.service";
import { Helper, OutputMain, PickType, PickValueConverter } from "./../../../shared/helper";
import { Config } from "./../../../shared/config";

import { ButtonEditorHelper } from "../../../shared/button-helper";
import { RadDataFormComponent } from "nativescript-pro-ui/dataform/angular";
import { android as androidApplication } from "tns-core-modules/application";

import { BarcodeScanService } from "../../../shared/barcode-scan.service";
import { RouterService } from "../../../shared/router.service";

@Component({
    moduleId: module.id,
    selector: "panel-output",
    templateUrl: "./panel-output.html",
    styleUrls: ["./panel-output-common.css", "./panel-output.css"],
    providers: []
})

export class PanelOutputComponent implements OnInit {

    public title: string;
    private _panelConfigOutput: any;
    private panelConfigOutputBackup: any;
    public panelId: number;
    public panelOutputId: number;
    public panelOutput: any;
    public outputMainProperties: any;
    public user_code_turns_output_on: any;
    public user_code_turns_output_off: any;
    public pendant_panic_alarm_to_output: any;
    public pendant_fire_alarm_to_output: any;
    public pendant_medical_alarm_to_output: any;

    public signals_to_output: any;

    public labelTextSize: number;

    public zone_alarm_assigned_to_output: any;
    public zone_stay_alarm_assigned_to_output: any;
    public zone_24_hour_alarm_assigned_to_output: any;
    public zone_tamper_assigned_to_output: any;
    public chime_zone_alarm_assigned_to_output: any;
    public armed_zone_entry_delay_assigned_to_output: any;
    public stay_mode_entry_delay_assigned_to_output: any;
    public zone_near_alarm_assigned_to_output: any;
    public zone_verified_alarm_assigned_to_output: any;

    public outputTypes: Array<PickType>;
    public outputTypeConverter: PickValueConverter;
    public outputTypeNames: Array<String>;
    public chimeResetModes: Array<PickType>;
    public chimeResetModeNames: Array<String>;
    public chimeResetModeConverter: PickValueConverter;

    public buttonBarcode: any;
    private _buttonEditorHelper;

    get panelConfigOutput() {
        return this._panelConfigOutput;
    }

    set panelConfigOutput(v: any) {
        this._panelConfigOutput = v;
    }

    public keys(object: any) {
        return Helper.keys(object);
    }


    constructor(private pageRoute: PageRoute, private panelOutputService: PanelOutputService, public routerService: RouterService, private barcodeScanService: BarcodeScanService) {
        this.pageRoute.activatedRoute
            .switchMap(activatedRoute => activatedRoute.params)
            .forEach((params) => {
                this.panelId = +params["panelId"];
                this.panelOutputId = +params["outputId"];
                this.title = "Panel " + this.panelId + ' > Outputs > ' + this.panelOutputService.getOutputNameById(this.panelId, this.panelOutputId);
            });
    }

    ngOnInit() {
        this.outputTypes = new Array<PickType>();
        for (let key in Config.outputTypes) {
            this.outputTypes.push(new PickType(+key, Config.outputTypes[+key]));
        }
        this.outputTypeConverter = new PickValueConverter(this.outputTypes);
        this.outputTypeNames = PickType.pickTypeNames(this.outputTypes);

        this.chimeResetModes = new Array<PickType>();
        for (let key in Config.chimeResetModes) {
            this.chimeResetModes.push(new PickType(+key, Config.chimeResetModes[+key]));
        }
        this.chimeResetModeConverter = new PickValueConverter(this.chimeResetModes);
        this.chimeResetModeNames = PickType.pickTypeNames(this.chimeResetModes);

        this.panelConfigOutput = this.panelOutputService.getOutput(this.panelId, this.panelOutputId);
        if (this.panelConfigOutput) {
            this.panelConfigOutputBackup = this.panelConfigOutput;
            this._deComposeConfigOutput();
        }
        this.labelTextSize = 16;
    }

    @ViewChild('outputMainPropertiesForm') outputMainPropertiesForm: RadDataFormComponent;

    private _deComposeConfigOutput() {
        this.panelOutput = Helper.deepCopyJsonObject(this.panelConfigOutput);
        delete this.panelOutput.user_code_turns_output_on;
        delete this.panelOutput.user_code_turns_output_off;
        delete this.panelOutput.pendant_panic_alarm_to_output;
        delete this.panelOutput.pendant_fire_alarm_to_output;
        delete this.panelOutput.pendant_medical_alarm_to_output;
        delete this.panelOutput.output_name;
        delete this.panelOutput.radio_outputs_list;
        delete this.panelOutput.zone_alarm_assigned_to_output;
        delete this.panelOutput.zone_stay_alarm_assigned_to_output;
        delete this.panelOutput.zone_24_hour_alarm_assigned_to_output;
        delete this.panelOutput.zone_tamper_assigned_to_output;
        delete this.panelOutput.chime_zone_alarm_assigned_to_output;
        delete this.panelOutput.armed_zone_entry_delay_assigned_to_output;
        delete this.panelOutput.stay_mode_entry_delay_assigned_to_output;
        delete this.panelOutput.zone_near_alarm_assigned_to_output;
        delete this.panelOutput.zone_verified_alarm_assigned_to_output;

        delete this.panelOutput.mains_fail_to_output;
        delete this.panelOutput.fuse_fail_to_output;
        delete this.panelOutput.batt_low_to_output;
        delete this.panelOutput.monitor_output_fail_to_output;
        delete this.panelOutput.output_tamper_alarm_to_output;
        delete this.panelOutput.communication_fail_to_output;
        delete this.panelOutput.radio_zone_supervized_fail_to_output;
        delete this.panelOutput.system_tamper_to_output;
        delete this.panelOutput.sensor_watch_to_output;
        delete this.panelOutput.duress_alarm_to_output;
        delete this.panelOutput.walk_test_pulse_to_output;

        this.user_code_turns_output_on = {};
        this.user_code_turns_output_off = {};
        this.pendant_panic_alarm_to_output = {};
        this.pendant_fire_alarm_to_output = {};
        this.pendant_medical_alarm_to_output = {};


        this.zone_alarm_assigned_to_output = {};
        this.zone_stay_alarm_assigned_to_output = {};
        this.zone_24_hour_alarm_assigned_to_output = {};
        this.zone_tamper_assigned_to_output = {};
        this.chime_zone_alarm_assigned_to_output = {};
        this.armed_zone_entry_delay_assigned_to_output = {};
        this.stay_mode_entry_delay_assigned_to_output = {};
        this.zone_near_alarm_assigned_to_output = {};
        this.zone_verified_alarm_assigned_to_output = {};


        this.signals_to_output = {};
        this.signals_to_output.mains_fail_to_output = this.panelConfigOutput.mains_fail_to_output;
        this.signals_to_output.fuse_fail_to_output = this.panelConfigOutput.fuse_fail_to_output;
        this.signals_to_output.batt_low_to_output = this.panelConfigOutput.batt_low_to_output;
        this.signals_to_output.monitor_output_fail_to_output = this.panelConfigOutput.monitor_output_fail_to_output;
        this.signals_to_output.output_tamper_alarm_to_output = this.panelConfigOutput.output_tamper_alarm_to_output;
        this.signals_to_output.communication_fail_to_output = this.panelConfigOutput.communication_fail_to_output;
        this.signals_to_output.radio_zone_supervized_fail_to_output = this.panelConfigOutput.radio_zone_supervized_fail_to_output;
        this.signals_to_output.system_tamper_to_output = this.panelConfigOutput.system_tamper_to_output;
        this.signals_to_output.sensor_watch_to_output = this.panelConfigOutput.sensor_watch_to_output;
        this.signals_to_output.duress_alarm_to_output = this.panelConfigOutput.duress_alarm_to_output;
        this.signals_to_output.walk_test_pulse_to_output = this.panelConfigOutput.walk_test_pulse_to_output;




        for (let outputId in this.panelConfigOutput.user_code_turns_output_on) {
            this.user_code_turns_output_on[outputId] = this.panelConfigOutput.user_code_turns_output_on[outputId]["assigned"];
        }
        for (let outputId in this.panelConfigOutput.user_code_turns_output_off) {
            this.user_code_turns_output_off[outputId] = this.panelConfigOutput.user_code_turns_output_off[outputId]["assigned"];
        }
        for (let outputId in this.panelConfigOutput.pendant_panic_alarm_to_output) {
            this.pendant_panic_alarm_to_output[outputId] = this.panelConfigOutput.pendant_panic_alarm_to_output[outputId]["assigned"];
        }
        for (let outputId in this.panelConfigOutput.pendant_fire_alarm_to_output) {
            this.pendant_fire_alarm_to_output[outputId] = this.panelConfigOutput.pendant_fire_alarm_to_output[outputId]["assigned"];
        }
        for (let outputId in this.panelConfigOutput.pendant_medical_alarm_to_output) {
            this.pendant_medical_alarm_to_output[outputId] = this.panelConfigOutput.pendant_medical_alarm_to_output[outputId]["assigned"];
        }

        for (let zoneId in this.panelConfigOutput.zone_alarm_assigned_to_output) {
            this.zone_alarm_assigned_to_output[zoneId] = this.panelConfigOutput.zone_alarm_assigned_to_output[zoneId]["assigned"];
            this.zone_stay_alarm_assigned_to_output[zoneId] = this.panelConfigOutput.zone_stay_alarm_assigned_to_output[zoneId]["assigned"];
            this.zone_24_hour_alarm_assigned_to_output[zoneId] = this.panelConfigOutput.zone_24_hour_alarm_assigned_to_output[zoneId]["assigned"];
            this.zone_tamper_assigned_to_output[zoneId] = this.panelConfigOutput.zone_tamper_assigned_to_output[zoneId]["assigned"];
            this.chime_zone_alarm_assigned_to_output[zoneId] = this.panelConfigOutput.chime_zone_alarm_assigned_to_output[zoneId]["assigned"];
            this.armed_zone_entry_delay_assigned_to_output[zoneId] = this.panelConfigOutput.armed_zone_entry_delay_assigned_to_output[zoneId]["assigned"];
            this.stay_mode_entry_delay_assigned_to_output[zoneId] = this.panelConfigOutput.stay_mode_entry_delay_assigned_to_output[zoneId]["assigned"];
            this.zone_near_alarm_assigned_to_output[zoneId] = this.panelConfigOutput.zone_near_alarm_assigned_to_output[zoneId]["assigned"];
            this.zone_verified_alarm_assigned_to_output[zoneId] = this.panelConfigOutput.zone_verified_alarm_assigned_to_output[zoneId]["assigned"];
        }

        this.outputMainProperties = new OutputMain();
        this.outputMainProperties.output_name = this.panelConfigOutput.output_name;
        this.outputMainProperties.radio_outputs_list = this.panelConfigOutput.radio_outputs_list;
        this.outputMainProperties.buttonBarcode = this.outputMainProperties.radio_outputs_list;
    }

    private _composeConfigOutput() {
        let user_code_turns_output_on = Helper.deepCopyJsonObject(this.panelConfigOutput.user_code_turns_output_on);
        let user_code_turns_output_off = Helper.deepCopyJsonObject(this.panelConfigOutput.user_code_turns_output_off);
        let pendant_panic_alarm_to_output = Helper.deepCopyJsonObject(this.panelConfigOutput.pendant_panic_alarm_to_output);
        let pendant_fire_alarm_to_output = Helper.deepCopyJsonObject(this.panelConfigOutput.pendant_fire_alarm_to_output);
        let pendant_medical_alarm_to_output = Helper.deepCopyJsonObject(this.panelConfigOutput.pendant_medical_alarm_to_output);
        let zone_alarm_assigned_to_output = Helper.deepCopyJsonObject(this.panelConfigOutput.zone_alarm_assigned_to_output);
        let zone_stay_alarm_assigned_to_output = Helper.deepCopyJsonObject(this.panelConfigOutput.zone_stay_alarm_assigned_to_output);
        let zone_24_hour_alarm_assigned_to_output = Helper.deepCopyJsonObject(this.panelConfigOutput.zone_24_hour_alarm_assigned_to_output);
        let zone_tamper_assigned_to_output = Helper.deepCopyJsonObject(this.panelConfigOutput.zone_tamper_assigned_to_output);
        let chime_zone_alarm_assigned_to_output = Helper.deepCopyJsonObject(this.panelConfigOutput.chime_zone_alarm_assigned_to_output);
        let armed_zone_entry_delay_assigned_to_output = Helper.deepCopyJsonObject(this.panelConfigOutput.armed_zone_entry_delay_assigned_to_output);
        let stay_mode_entry_delay_assigned_to_output = Helper.deepCopyJsonObject(this.panelConfigOutput.stay_mode_entry_delay_assigned_to_output);
        let zone_near_alarm_assigned_to_output = Helper.deepCopyJsonObject(this.panelConfigOutput.zone_near_alarm_assigned_to_output);
        let zone_verified_alarm_assigned_to_output = Helper.deepCopyJsonObject(this.panelConfigOutput.zone_verified_alarm_assigned_to_output);

        this.panelConfigOutput = Helper.deepCopyJsonObject(this.panelOutput);
        for (let userId in this.user_code_turns_output_on) {
            user_code_turns_output_on[userId]["assigned"] = this.user_code_turns_output_on[userId];
        }
        for (let userId in this.user_code_turns_output_off) {
            user_code_turns_output_off[userId]["assigned"] = this.user_code_turns_output_off[userId];
        }
        for (let userId in this.pendant_panic_alarm_to_output) {
            pendant_panic_alarm_to_output[userId]["assigned"] = this.pendant_panic_alarm_to_output[userId];
        }
        for (let userId in this.pendant_fire_alarm_to_output) {
            pendant_fire_alarm_to_output[userId]["assigned"] = this.pendant_fire_alarm_to_output[userId];
        }
        for (let userId in this.pendant_medical_alarm_to_output) {
            pendant_medical_alarm_to_output[userId]["assigned"] = this.pendant_medical_alarm_to_output[userId];
        }

        for (let zoneId in this.zone_alarm_assigned_to_output) {
            zone_alarm_assigned_to_output[zoneId]["assigned"] = this.zone_alarm_assigned_to_output[zoneId];
            zone_stay_alarm_assigned_to_output[zoneId]["assigned"] = this.zone_stay_alarm_assigned_to_output[zoneId];
            zone_24_hour_alarm_assigned_to_output[zoneId]["assigned"] = this.zone_24_hour_alarm_assigned_to_output[zoneId];
            zone_tamper_assigned_to_output[zoneId]["assigned"] = this.zone_tamper_assigned_to_output[zoneId];
            chime_zone_alarm_assigned_to_output[zoneId]["assigned"] = this.chime_zone_alarm_assigned_to_output[zoneId];
            armed_zone_entry_delay_assigned_to_output[zoneId]["assigned"] = this.armed_zone_entry_delay_assigned_to_output[zoneId];
            stay_mode_entry_delay_assigned_to_output[zoneId]["assigned"] = this.stay_mode_entry_delay_assigned_to_output[zoneId];
            zone_near_alarm_assigned_to_output[zoneId]["assigned"] = this.zone_near_alarm_assigned_to_output[zoneId];
            zone_verified_alarm_assigned_to_output[zoneId]["assigned"] = this.zone_verified_alarm_assigned_to_output[zoneId];
        }

        delete this.outputMainProperties.buttonBarcode;
        this.panelConfigOutput.user_code_turns_output_on = Helper.deepCopyJsonObject(user_code_turns_output_on);
        this.panelConfigOutput.user_code_turns_output_off = Helper.deepCopyJsonObject(user_code_turns_output_off);
        this.panelConfigOutput.pendant_panic_alarm_to_output = Helper.deepCopyJsonObject(pendant_panic_alarm_to_output);
        this.panelConfigOutput.pendant_fire_alarm_to_output = Helper.deepCopyJsonObject(pendant_fire_alarm_to_output);
        this.panelConfigOutput.pendant_medical_alarm_to_output = Helper.deepCopyJsonObject(pendant_medical_alarm_to_output);
        this.panelConfigOutput.output_name = this.outputMainProperties.output_name;
        this.panelConfigOutput.radio_outputs_list = this.outputMainProperties.radio_outputs_list;
        this.panelConfigOutput.zone_alarm_assigned_to_output = Helper.deepCopyJsonObject(zone_alarm_assigned_to_output);
        this.panelConfigOutput.zone_stay_alarm_assigned_to_output = Helper.deepCopyJsonObject(zone_stay_alarm_assigned_to_output);
        this.panelConfigOutput.zone_24_hour_alarm_assigned_to_output = Helper.deepCopyJsonObject(zone_24_hour_alarm_assigned_to_output);
        this.panelConfigOutput.zone_tamper_assigned_to_output = Helper.deepCopyJsonObject(zone_tamper_assigned_to_output);
        this.panelConfigOutput.chime_zone_alarm_assigned_to_output = Helper.deepCopyJsonObject(chime_zone_alarm_assigned_to_output);
        this.panelConfigOutput.armed_zone_entry_delay_assigned_to_output = Helper.deepCopyJsonObject(armed_zone_entry_delay_assigned_to_output);
        this.panelConfigOutput.stay_mode_entry_delay_assigned_to_output = Helper.deepCopyJsonObject(stay_mode_entry_delay_assigned_to_output);
        this.panelConfigOutput.zone_near_alarm_assigned_to_output = Helper.deepCopyJsonObject(zone_near_alarm_assigned_to_output);
        this.panelConfigOutput.zone_verified_alarm_assigned_to_output = Helper.deepCopyJsonObject(zone_verified_alarm_assigned_to_output);

        this.panelConfigOutput.mains_fail_to_output = this.signals_to_output.mains_fail_to_output;
        this.panelConfigOutput.fuse_fail_to_output = this.signals_to_output.fuse_fail_to_output;
        this.panelConfigOutput.batt_low_to_output = this.signals_to_output.batt_low_to_output;
        this.panelConfigOutput.monitor_output_fail_to_output = this.signals_to_output.monitor_output_fail_to_output;
        this.panelConfigOutput.output_tamper_alarm_to_output = this.signals_to_output.output_tamper_alarm_to_output;
        this.panelConfigOutput.communication_fail_to_output = this.signals_to_output.communication_fail_to_output;
        this.panelConfigOutput.radio_zone_supervized_fail_to_output = this.signals_to_output.radio_zone_supervized_fail_to_output;
        this.panelConfigOutput.system_tamper_to_output = this.signals_to_output.system_tamper_to_output;
        this.panelConfigOutput.sensor_watch_to_output = this.signals_to_output.sensor_watch_to_output;
        this.panelConfigOutput.duress_alarm_to_output = this.signals_to_output.duress_alarm_to_output;
        this.panelConfigOutput.walk_test_pulse_to_output = this.signals_to_output.walk_test_pulse_to_output;

    }

    public save() {
        this.outputMainPropertiesForm.dataForm.commitAll();
        this.routerService.isLoading = true;
        this._composeConfigOutput();
        this.panelOutputService.saveOutput(this.panelId, this.panelConfigOutput)
            .then(res => {
                this.routerService.simpleBackward('/panel-detail/' + this.panelId + '/outputs/');
            })
            .catch(error => {
                this.panelConfigOutput = this.panelConfigOutputBackup;
                this.panelOutputService.restoreOutput(this.panelId, this.panelConfigOutputBackup);
                alert(error);
                this.routerService.isLoading = false;
            });
    }

    public onNavBackBtnTap() {
        this.routerService.isLoading = true;
        this.routerService.simpleBackward('/panel-detail/' + this.panelId + '/outputs/');
    }

    public editorNeedsView(args) {
        if (androidApplication) {
            this._buttonEditorHelper = new ButtonEditorHelper();
            this._buttonEditorHelper.editor = args.object;
            let androidEditorView: android.widget.Button = new android.widget.Button(args.context);
            let that = this;
            androidEditorView.setOnClickListener(new android.view.View.OnClickListener({
                onClick(view: android.view.View) {
                    that.handleTap(view, args.object);
                }
            }));
            args.view = androidEditorView;
            this.updateEditorValue(androidEditorView, this.outputMainProperties.radio_outputs_list);
        } else {
            this._buttonEditorHelper = new ButtonEditorHelper();
            this._buttonEditorHelper.editor = args.object;
            let iosEditorView = UIButton.buttonWithType(UIButtonType.System);
            iosEditorView.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left;
            iosEditorView.addTargetActionForControlEvents(this._buttonEditorHelper, "handleTap:", UIControlEvents.TouchUpInside);
            args.view = iosEditorView;
        }
    }

    public editorHasToApplyValue(args) {
        this._buttonEditorHelper.updateEditorValue(args.view, args.value);
    }

    public editorNeedsValue(args) {
        args.value = this._buttonEditorHelper.buttonValue;
    }

    public updateEditorValue(editorView, value) {
        this._buttonEditorHelper.buttonValue = value;
        editorView.setText("Tap to scan pendant barcode");
    }

    public handleTap(editorView, editor) {
        this.barcodeScanService.scanBarcode()
            .then((result) => {
                    this.updateEditorValue(editorView, result.text);
                    editor.notifyValueChanged();
                    /* TODO
                    https://github.com/telerik/nativescript-ui-feedback/issues/344
                    * */
                    Helper.reloadRadDataFormProperties(this, 'outputMainPropertiesForm', 'outputMainProperties', {
                        "radio_outputs_list": +result.text
                    });
                }, (errorMessage) => {
                    alert("No scan. " + errorMessage);
                    console.log("No scan. " + errorMessage);
                }
            )
            .catch((error) => alert('Error scan barcode.. ' + error));
    }
}