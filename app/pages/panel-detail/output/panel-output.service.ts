import { Injectable } from "@angular/core";
import {PanelConfigService} from "../../../shared/panel/panel-config.service";
import {Helper} from "../../../shared/helper";

@Injectable()
export class PanelOutputService {

    constructor(private panelConfigService: PanelConfigService) {}

    public saveOutput(panelId: number, output: any) {
        this.saveOutputLocally(panelId, output);
        return this.panelConfigService.saveConfig();
    }
    public restoreOutput(panelId: number, output: any) {
        this.saveOutputLocally(panelId, output);
    }
    public getOutputNameById(panelId: number, outputId: number) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        return panelConfig.output_name[outputId];
    }
    public saveOutputLocally(panelId: number, output: any) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        let keysToIgnore = ['id', 'user_code_turns_output_on', 'user_code_turns_output_off',
                            'pendant_panic_alarm_to_output', 'pendant_fire_alarm_to_output', 'pendant_medical_alarm_to_output',
                            'zone_alarm_assigned_to_output', 'zone_stay_alarm_assigned_to_output',
                            'zone_24_hour_alarm_assigned_to_output','zone_tamper_assigned_to_output', 'chime_zone_alarm_assigned_to_output',
                            'armed_zone_entry_delay_assigned_to_output', 'stay_mode_entry_delay_assigned_to_output',
                            'zone_near_alarm_assigned_to_output', 'zone_verified_alarm_assigned_to_output'];
        for (let key in output) {
            if(keysToIgnore.indexOf(key) === -1) {
                panelConfig[key][output.id] = output[key];
            }
        }
        for (let userId in output.user_code_turns_output_on) {
            panelConfig.user_code_turns_output_on[output.id][userId] = output.user_code_turns_output_on[userId]['assigned'];
        }
        for (let userId in output.user_code_turns_output_off) {
            panelConfig.user_code_turns_output_off[output.id][userId] = output.user_code_turns_output_off[userId]['assigned'];
        }
        for (let userId in output.pendant_panic_alarm_to_output) {
            panelConfig.pendant_panic_alarm_to_output[output.id][userId] = output.pendant_panic_alarm_to_output[userId]['assigned'];
        }
        for (let userId in output.pendant_fire_alarm_to_output) {
            panelConfig.pendant_fire_alarm_to_output[output.id][userId] = output.pendant_fire_alarm_to_output[userId]['assigned'];
        }
        for (let userId in output.pendant_medical_alarm_to_output) {
            panelConfig.pendant_medical_alarm_to_output[output.id][userId] = output.pendant_medical_alarm_to_output[userId]['assigned'];
        }
        for (let zoneId in output.zone_alarm_assigned_to_output) {
            panelConfig.zone_alarm_assigned_to_output[output.id][zoneId] = output.zone_alarm_assigned_to_output[zoneId]['assigned'];
            panelConfig.zone_stay_alarm_assigned_to_output[output.id][zoneId] = output.zone_stay_alarm_assigned_to_output[zoneId]['assigned'];
            panelConfig.zone_24_hour_alarm_assigned_to_output[output.id][zoneId] = output.zone_24_hour_alarm_assigned_to_output[zoneId]['assigned'];
            panelConfig.zone_tamper_assigned_to_output[output.id][zoneId] = output.zone_tamper_assigned_to_output[zoneId]['assigned'];
            panelConfig.chime_zone_alarm_assigned_to_output[output.id][zoneId] = output.chime_zone_alarm_assigned_to_output[zoneId]['assigned'];
            panelConfig.armed_zone_entry_delay_assigned_to_output[output.id][zoneId] = output.armed_zone_entry_delay_assigned_to_output[zoneId]['assigned'];
            panelConfig.stay_mode_entry_delay_assigned_to_output[output.id][zoneId] = output.stay_mode_entry_delay_assigned_to_output[zoneId]['assigned'];
            panelConfig.zone_near_alarm_assigned_to_output[output.id][zoneId] = output.zone_near_alarm_assigned_to_output[zoneId]['assigned'];
            panelConfig.zone_verified_alarm_assigned_to_output[output.id][zoneId] = output.zone_verified_alarm_assigned_to_output[zoneId]['assigned'];
        }
        this.panelConfigService.saveConfigLocally(panelConfig);
    }
    public getOutput(panelId: number, outputId: number) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        if(panelConfig) {
            let user_code_turns_output_on = {};
            let user_code_turns_output_off = {};
            let zone_alarm_assigned_to_output = {};
            let zone_stay_alarm_assigned_to_output = {};
            let zone_24_hour_alarm_assigned_to_output = {};
            let zone_tamper_assigned_to_output = {};
            let chime_zone_alarm_assigned_to_output = {};
            let armed_zone_entry_delay_assigned_to_output = {};
            let stay_mode_entry_delay_assigned_to_output = {};
            let zone_near_alarm_assigned_to_output = {};
            let zone_verified_alarm_assigned_to_output = {};


            let pendant_panic_alarm_to_output = {};
            let pendant_fire_alarm_to_output = {};
            let pendant_medical_alarm_to_output = {};


            panelConfig.user_code.map((element, userId, array) => {
                if (!Helper.isEmpty(element)) {
                    user_code_turns_output_on[userId] = {
                        name: panelConfig.user_name[userId],
                        assigned: panelConfig.user_code_turns_output_on[outputId][userId]
                    };
                    user_code_turns_output_off[userId] = {
                        name: panelConfig.user_name[userId],
                        assigned: panelConfig.user_code_turns_output_off[outputId][userId]
                    };
                    pendant_panic_alarm_to_output[userId] = {
                        name: panelConfig.user_name[userId],
                        assigned: panelConfig.pendant_panic_alarm_to_output[outputId][userId]
                    };
                    pendant_fire_alarm_to_output[userId] = {
                        name: panelConfig.user_name[userId],
                        assigned: panelConfig.pendant_fire_alarm_to_output[outputId][userId]
                    };
                    pendant_medical_alarm_to_output[userId] = {
                        name: panelConfig.user_name[userId],
                        assigned: panelConfig.pendant_medical_alarm_to_output[outputId][userId]
                    };
                }
            });


            panelConfig.radio_zones_list.map((element, zoneId, array) => {
                if (!Helper.isEmpty(element)) {
                    zone_alarm_assigned_to_output[zoneId] = {
                        name: panelConfig.zone_name[zoneId],
                        assigned: panelConfig.zone_alarm_assigned_to_output[outputId][zoneId]
                    };
                    zone_stay_alarm_assigned_to_output[zoneId] = {
                        name: panelConfig.zone_name[zoneId],
                        assigned: panelConfig.zone_stay_alarm_assigned_to_output[outputId][zoneId]
                    };
                    zone_24_hour_alarm_assigned_to_output[zoneId] = {
                        name: panelConfig.zone_name[zoneId],
                        assigned: panelConfig.zone_24_hour_alarm_assigned_to_output[outputId][zoneId]
                    };
                    zone_tamper_assigned_to_output[zoneId] = {
                        name: panelConfig.zone_name[zoneId],
                        assigned: panelConfig.zone_tamper_assigned_to_output[outputId][zoneId]
                    };
                    chime_zone_alarm_assigned_to_output[zoneId] = {
                        name: panelConfig.zone_name[zoneId],
                        assigned: panelConfig.chime_zone_alarm_assigned_to_output[outputId][zoneId]
                    };
                    armed_zone_entry_delay_assigned_to_output[zoneId] = {
                        name: panelConfig.zone_name[zoneId],
                        assigned: panelConfig.armed_zone_entry_delay_assigned_to_output[outputId][zoneId]
                    };
                    stay_mode_entry_delay_assigned_to_output[zoneId] = {
                        name: panelConfig.zone_name[zoneId],
                        assigned: panelConfig.stay_mode_entry_delay_assigned_to_output[outputId][zoneId]
                    };
                    zone_near_alarm_assigned_to_output[zoneId] = {
                        name: panelConfig.zone_name[zoneId],
                        assigned: panelConfig.zone_near_alarm_assigned_to_output[outputId][zoneId]
                    };
                    zone_verified_alarm_assigned_to_output[zoneId] = {
                        name: panelConfig.zone_name[zoneId],
                        assigned: panelConfig.zone_verified_alarm_assigned_to_output[outputId][zoneId]
                    };
                }
            });

            return {
                id: outputId,
                output_name: panelConfig.output_name[outputId],
                radio_outputs_list: panelConfig.radio_outputs_list[outputId],
                invert_output: panelConfig.invert_output[outputId],
                temporary_output_disable: panelConfig.temporary_output_disable[outputId],
                lockout_output: panelConfig.lockout_output[outputId],
                pulse_output_on_kiss_off_following_arming: panelConfig.pulse_output_on_kiss_off_following_arming[outputId],
                output_disable_during_disarm: panelConfig.output_disable_during_disarm[outputId],
                disable_during_alarm_report_delay: panelConfig.disable_during_alarm_report_delay[outputId],
                mute_for_10_sec_on_key_press_if_alarm: panelConfig.mute_for_10_sec_on_key_press_if_alarm[outputId],
                enable_output_monitoring: panelConfig.enable_output_monitoring[outputId],
                enable_mute: panelConfig.enable_mute[outputId],
                alt_flags_output_type: panelConfig.alt_flags_output_type[outputId],
                //alt_flags_chime_reset_mode: panelConfig.alt_flags_chime_reset_mode[outputId],
                user_code_turns_output_on: user_code_turns_output_on,
                user_code_turns_output_off: user_code_turns_output_off,
                mains_fail_to_output: panelConfig.mains_fail_to_output[outputId],
                fuse_fail_to_output: panelConfig.fuse_fail_to_output[outputId],
                batt_low_to_output: panelConfig.batt_low_to_output[outputId],
                monitor_output_fail_to_output: panelConfig.monitor_output_fail_to_output[outputId],
                output_tamper_alarm_to_output: panelConfig.output_tamper_alarm_to_output[outputId],
                communication_fail_to_output: panelConfig.communication_fail_to_output[outputId],
                radio_zone_supervized_fail_to_output: panelConfig.radio_zone_supervized_fail_to_output[outputId],
                system_tamper_to_output: panelConfig.system_tamper_to_output[outputId],
                sensor_watch_to_output: panelConfig.sensor_watch_to_output[outputId],
                duress_alarm_to_output: panelConfig.duress_alarm_to_output[outputId],
                walk_test_pulse_to_output: panelConfig.walk_test_pulse_to_output[outputId],
                output_delay_time: panelConfig.output_delay_time[outputId],
                output_pulse_time: panelConfig.output_pulse_time[outputId],
                output_reset_time: panelConfig.output_reset_time[outputId],
                zone_alarm_assigned_to_output: zone_alarm_assigned_to_output,
                zone_stay_alarm_assigned_to_output: zone_stay_alarm_assigned_to_output,
                zone_24_hour_alarm_assigned_to_output: zone_24_hour_alarm_assigned_to_output,
                zone_tamper_assigned_to_output: zone_tamper_assigned_to_output,
                chime_zone_alarm_assigned_to_output: chime_zone_alarm_assigned_to_output,
                armed_zone_entry_delay_assigned_to_output: armed_zone_entry_delay_assigned_to_output,
                stay_mode_entry_delay_assigned_to_output: stay_mode_entry_delay_assigned_to_output,
                zone_near_alarm_assigned_to_output: zone_near_alarm_assigned_to_output,
                zone_verified_alarm_assigned_to_output: zone_verified_alarm_assigned_to_output,
                pendant_panic_alarm_to_output: pendant_panic_alarm_to_output,
                pendant_fire_alarm_to_output: pendant_fire_alarm_to_output,
                pendant_medical_alarm_to_output: pendant_medical_alarm_to_output
            }
        }
        else {
            return {};
        }

    }
}