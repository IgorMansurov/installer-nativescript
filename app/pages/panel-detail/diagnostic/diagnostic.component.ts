import { Component, OnInit } from "@angular/core";
import { PageRoute } from "nativescript-angular/router";
import { RouterService } from "../../../shared/router.service";
import { DiagnosticService } from "./diagnostic.service";

@Component({
    moduleId: module.id,
    selector: "diagnostic",
    templateUrl: "./diagnostic.html",
    styleUrls: ["./diagnostic-common.css", "./diagnostic.css"],
    providers: []
})

export class DiagnosticComponent implements OnInit {

    public title: string;
    public panelId: number;
    public diagnosticEthernetDataForm: any;
    public diagnosticGsmDataForm: any;
    public diagnosticWifiDataForm: any;
    public diagnosticRadioDataForm: any;

    public isLoading: boolean;
    public labelTextSize: number;

    constructor(private pageRoute: PageRoute, private diagnosticService: DiagnosticService, public routerService: RouterService) {
        this.routerService.isLoading = true;
        this.pageRoute.activatedRoute
            .switchMap(activatedRoute => activatedRoute.params)
            .forEach((params) => {
                this.panelId = +params["panelId"];
                this.title = "Panel " + this.panelId + ' > Diagnostic';
            });
    }

    ngOnInit() {
        this.labelTextSize = 16;
    }

    public diagnoseCommunication() {
        this.diagnosticService.getDiagnostic(this.panelId, 'ethernet')
            .then((configDiagnostic) => {
                this.diagnosticEthernetDataForm = configDiagnostic;
            })
            .catch (error => error);

        this.diagnosticService.getDiagnostic(this.panelId, 'gsm')
            .then((configDiagnostic) => {
                this.diagnosticGsmDataForm = configDiagnostic;
            })
            .catch (error => error);

        this.diagnosticService.getDiagnostic(this.panelId, 'wifi')
            .then((configDiagnostic) => {
                this.diagnosticWifiDataForm = configDiagnostic;
            })
            .catch (error => error);

        this.diagnosticService.getDiagnostic(this.panelId, 'radio')
            .then((configDiagnostic) => {
                this.diagnosticRadioDataForm = configDiagnostic;
            })
            .catch (error => error);
    }

    public onNavBackBtnTap() {
        this.routerService.isLoading = true;
        this.routerService.simpleBackward('/panel-detail/' + this.panelId);
    }
}