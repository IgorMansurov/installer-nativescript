import { Injectable } from "@angular/core";
import {PanelConfigService} from "../../../shared/panel/panel-config.service";

@Injectable()
export class DiagnosticService {

    constructor(private panelConfigService: PanelConfigService) {}

    public getDiagnostic(panelId: number, communicationType: string) {
        return this.panelConfigService.getDiagnosticConfig(panelId, communicationType);
    }
}