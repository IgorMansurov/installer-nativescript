import { Component, OnInit } from "@angular/core";
import { RouterService } from "../../../shared/router.service";
import { PageRoute } from "nativescript-angular/router";
import { ReportChannelsService } from "./report-channels.service";

@Component({
    moduleId: module.id,
    selector: "report-channels",
    templateUrl: "./report-channels.html",
    styleUrls: ["./report-channels-common.css", "./report-channels.css"],
    providers: []
})
export class ReportChannelsComponent implements OnInit {

    public reportChannels: any;
    public panelId: number;
    public title: string;

    /*
      TODO: https://github.com/bradmartin/nativescript-floatingactionbutton/issues/61
    */

    constructor(private reportChannelsService: ReportChannelsService, private pageRoute: PageRoute, public routerService: RouterService) {
        this.routerService.isLoading = true;
        this.pageRoute.activatedRoute
            .switchMap(activatedRoute => activatedRoute.params)
            .forEach((params) => {
                this.panelId = +params["panelId"];
                this.title = "Panel " + this.panelId + ' > Report Channels';
            });
    }

    ngOnInit() {
        this.reportChannels = this.reportChannelsService.getReportChannelList(this.panelId);
    }

    public showAddOutputButton() {
        return this.reportChannelsService.quantityEmptyChannels(this.panelId) > 0;
    }

    public editReportChannel(reportChannelId: number = -1) {
        this.routerService.isLoading = true;
        if(reportChannelId === -1) {
          reportChannelId = this.reportChannelsService.getFirstEmptyChannel(this.panelId);
        }
        this.routerService.simpleForward('/panel-detail/' + this.panelId + '/report-channels/' + reportChannelId);
    }

    public onNavBackBtnTap() {
        this.routerService.isLoading = true;
        this.routerService.simpleBackward('/panel-detail/' + this.panelId);
    }
}