import { Injectable } from "@angular/core";
import { PanelConfigService } from "../../../shared/panel/panel-config.service";
import { Config } from "../../../shared/config";

@Injectable()
export class ReportChannelsService {

    constructor(private panelConfigService: PanelConfigService) {
    }

    public getReportChannelList(panelId: number) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        let reportChannels = new Array;
        if(panelConfig) {
            panelConfig.active_channel.map((element, reportChannelId, array) => {
                (element !== false) ? reportChannels.push(this.getChannelIdAndNameByIndex(panelId, reportChannelId)) : '';
            });
        }
        return reportChannels;
    }

    public quantityEmptyChannels(panelId: number) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        if(panelConfig) {
            let emptyChannels = this.panelConfigService.getConfig(panelId).active_channel.filter((element, index, array) => {
                return (element === false);
            });
            return emptyChannels.length;
        }
        else {
            return 0;
        }
    }

    public getFirstEmptyChannel(panelId: number) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        if(panelConfig) {
            return this.panelConfigService.getConfig(panelId).active_channel.findIndex((element, index, array) => {
                return (element === false);
            });
        }
        else {
            return null;
        }
    }

    private getChannelIdAndNameByIndex(panelId: number, reportChannelId: number) {
        return {
            id: reportChannelId,
            channel_name: Config.channelTypes[this.panelConfigService.getConfig(panelId).alt_flags_channel_type[reportChannelId]] + ' (' + String(reportChannelId + 1) + ')'
        };
    }
}