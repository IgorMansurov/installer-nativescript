import { Injectable } from "@angular/core";
import { PanelConfigService } from "./../../../shared/panel/panel-config.service";
import { Helper } from "./../../../shared/helper";

@Injectable()
export class KeypadsService {

    constructor(private panelConfigService: PanelConfigService) {
    }

    public getKeypadList(panelId: number) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        let keypads = new Array;
        if(panelConfig) {
            panelConfig.keypads_list.map((element, index, array) => {
                !Helper.isEmpty(element) ? keypads.push({id: index, keypads_list: element}) : '';
            });
        }
        return keypads;
    }

    public quantityEmptyKeypads(panelId: number) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        if(panelConfig) {
            let emptyKeypads = panelConfig.keypads_list.filter((element, index, array) => {
                return Helper.isEmpty(element);
            });
            return emptyKeypads.length;
        }
        else {
            return 0;
        }
    }

    public getFirstEmptyKeypad(panelId: number) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        if(panelConfig) {
            return this.panelConfigService.getConfig(panelId).keypads_list.findIndex((element, index, array) => {
                return Helper.isEmpty(element);
            });
        }
        else {
            return null;
        }
    }
}