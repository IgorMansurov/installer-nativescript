import { Component, OnInit } from "@angular/core";
import { PageRoute } from "nativescript-angular/router";
import { KeypadsService } from "./keypads.service";
import { RouterService } from "../../../shared/router.service";

@Component({
    moduleId: module.id,
    selector: "keypads",
    templateUrl: "./keypads.html",
    styleUrls: ["./keypads-common.css", "./keypads.css"],
    providers: []
})
export class KeypadsComponent implements OnInit {

    public keypads: any;
    public panelId: number;
    public title: string;

    constructor(private keypadsService: KeypadsService, private pageRoute: PageRoute, public routerService: RouterService) {
        this.routerService.isLoading = true;
        this.pageRoute.activatedRoute
            .switchMap(activatedRoute => activatedRoute.params)
            .forEach((params) => {
                this.panelId = +params["panelId"];
                this.title = "Panel " + this.panelId + ' > Keypads';
            });
    }

    ngOnInit() {
        this.keypads = this.keypadsService.getKeypadList(this.panelId);
    }

    public showAddOutputButton() {
        return this.keypadsService.quantityEmptyKeypads(this.panelId) > 0;
    }

    public editKeypad(keypadId: number = -1) {
        this.routerService.isLoading = true;
        if (keypadId === -1) {
            keypadId = this.keypadsService.getFirstEmptyKeypad(this.panelId);
        }
        this.routerService.simpleForward('/panel-detail/' + this.panelId + '/keypads/' + keypadId);
    }

    public onNavBackBtnTap() {
        this.routerService.isLoading = true;
        this.routerService.simpleBackward('/panel-detail/' + this.panelId);
    }

    public increaseIndex(index: any) {
       return String(Number(index) + 1);
    }
}