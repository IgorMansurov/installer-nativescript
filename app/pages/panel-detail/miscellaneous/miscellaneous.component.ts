import { Component, OnInit } from "@angular/core";
import { PageRoute } from "nativescript-angular/router";
import { RouterService } from "../../../shared/router.service";
import { MiscellaneousService } from "./miscellaneous.service";
import { Helper, PickType, PickValueConverter } from "./../../../shared/helper";
import { Config } from "./../../../shared/config";

@Component({
    moduleId: module.id,
    selector: "miscellaneous",
    templateUrl: "./miscellaneous.html",
    styleUrls: ["./miscellaneous-common.css", "./miscellaneous.css"],
    providers: []
})

export class MiscellaneousComponent implements OnInit {

    public title: string;
    public configMiscellaneous: any;
    public configMiscellaneousBackup: any;
    public panelId: number;
    public miscellaneousDataForm: any;

    public systemdatetime: any;
    public delays: any;
    public timers: any;

    public dateFormats: Array<PickType>;
    public dateFormatConverter: PickValueConverter;
    public dateFormatNames: Array<String>;

    public labelTextSize: number;

    public keys(object: any) {
        return Helper.keys(object);
    }

    constructor(private pageRoute: PageRoute, private miscellaneousService: MiscellaneousService, public routerService: RouterService) {
        this.routerService.isLoading = true;
        this.pageRoute.activatedRoute
            .switchMap(activatedRoute => activatedRoute.params)
            .forEach((params) => {
                this.panelId = +params["panelId"];
                this.title = "Panel " + this.panelId + ' > Miscellaneous';
            });
    }

    ngOnInit() {
        this.dateFormats = new Array<PickType>();
        for(let key in Config.dateFormats) {
            this.dateFormats.push(new PickType(+key, Config.dateFormats[+key]));
        }
        this.dateFormatConverter = new PickValueConverter(this.dateFormats);
        this.dateFormatNames = PickType.pickTypeNames(this.dateFormats);

        this.configMiscellaneous = this.miscellaneousService.getMiscellaneous(this.panelId);
        if(this.configMiscellaneous) {
            this.configMiscellaneousBackup = this.configMiscellaneous;
            this._deComposeConfigMiscellaneous();
        }
        this.labelTextSize = 16;
    }

    private _deComposeConfigMiscellaneous() {
        this.miscellaneousDataForm = Helper.deepCopyJsonObject(this.configMiscellaneous);
        delete this.miscellaneousDataForm.alarm_reporting_delay;
        delete this.miscellaneousDataForm.mains_fail_reporting_delay;
        delete this.miscellaneousDataForm.communication_fail_reporting_delay;
        delete this.miscellaneousDataForm.radio_zone_supervised_time;
        delete this.miscellaneousDataForm.two_trigger_time;
        delete this.miscellaneousDataForm.Daylight_Saving;
        delete this.miscellaneousDataForm.gmt;
        delete this.miscellaneousDataForm.alt_flags_date_format;

        this.systemdatetime = {};
        this.delays = {};
        this.timers = {};

        this.systemdatetime.Daylight_Saving = this.configMiscellaneous.Daylight_Saving;
        this.systemdatetime.gmt = this.configMiscellaneous.gmt;
        this.systemdatetime.alt_flags_date_format = this.configMiscellaneous.alt_flags_date_format;


        this.delays.alarm_reporting_delay = this.configMiscellaneous.alarm_reporting_delay;
        this.delays.mains_fail_reporting_delay = this.configMiscellaneous.mains_fail_reporting_delay;
        this.delays.communication_fail_reporting_delay = this.configMiscellaneous.communication_fail_reporting_delay;
        this.timers.radio_zone_supervised_time = this.configMiscellaneous.radio_zone_supervised_time;
        this.timers.two_trigger_time = this.configMiscellaneous.two_trigger_time;
    }

    private _composeConfigZone() {
        this.configMiscellaneous = Helper.deepCopyJsonObject(this.miscellaneousDataForm);

        this.configMiscellaneous.Daylight_Saving = this.systemdatetime.Daylight_Saving;
        this.configMiscellaneous.gmt = this.systemdatetime.gmt;
        this.configMiscellaneous.alt_flags_date_format = this.systemdatetime.alt_flags_date_format;
        this.configMiscellaneous.alarm_reporting_delay = this.delays.alarm_reporting_delay;
        this.configMiscellaneous.mains_fail_reporting_delay = this.delays.mains_fail_reporting_delay;
        this.configMiscellaneous.communication_fail_reporting_delay = this.delays.communication_fail_reporting_delay;
        this.configMiscellaneous.radio_zone_supervised_time = this.timers.radio_zone_supervised_time;
        this.configMiscellaneous.two_trigger_time = this.timers.two_trigger_time;
    }

    public save() {
        this.routerService.isLoading = true;
        this._composeConfigZone();
        this.miscellaneousService.saveMiscellaneous(this.panelId, this.configMiscellaneous)
            .then(res => {
                this.routerService.simpleBackward('/panel-detail/' + this.panelId);
            })
            .catch(error => {
                this.configMiscellaneous = this.configMiscellaneousBackup;
                this.miscellaneousService.restoreMiscellaneous(this.panelId, this.configMiscellaneousBackup);
                alert(error);
                this.routerService.isLoading = false;
            });
    }

    public onNavBackBtnTap() {
        this.routerService.isLoading = true;
        this.routerService.simpleBackward('/panel-detail/' + this.panelId);
    }
}