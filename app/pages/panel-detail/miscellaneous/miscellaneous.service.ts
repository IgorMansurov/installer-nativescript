import { Injectable } from "@angular/core";
import {PanelConfigService} from "../../../shared/panel/panel-config.service";

@Injectable()
export class MiscellaneousService {

    constructor(private panelConfigService: PanelConfigService) {}

    public saveMiscellaneous(panelId: number, miscellaneous: any) {
        this.saveMiscellaneousLocally(panelId, miscellaneous);
        return this.panelConfigService.saveConfig();
    }
    public restoreMiscellaneous(panelId: number, miscellaneous: any) {
        this.saveMiscellaneousLocally(panelId, miscellaneous);
    }
    public saveMiscellaneousLocally(panelId: number, miscellaneous: any) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        let keysToIgnore = [];

        for (let key in miscellaneous) {
            if(keysToIgnore.indexOf(key) === -1) {
                panelConfig[key] = miscellaneous[key];
            }
        }

        this.panelConfigService.saveConfigLocally(panelConfig);
    }

    public getMiscellaneous(panelId: number) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        if(panelConfig) {
            return {
                Daylight_Saving: panelConfig.Daylight_Saving,
                gmt: panelConfig.gmt,
                alt_flags_date_format: panelConfig.alt_flags_date_format,
                radio_zone_supervised_time: panelConfig.radio_zone_supervised_time,
                two_trigger_time: panelConfig.two_trigger_time,
                alarm_reporting_delay: panelConfig.alarm_reporting_delay,
                mains_fail_reporting_delay: panelConfig.mains_fail_reporting_delay,
                communication_fail_reporting_delay: panelConfig.communication_fail_reporting_delay,
                installer_code: panelConfig.installer_code,
                duress_digit: panelConfig.duress_digit,
                Disable_mains_fail_test: panelConfig.Disable_mains_fail_test,
                Installer_Lockout: panelConfig.Installer_Lockout,
                buzzer_reset_time: panelConfig.buzzer_reset_time,
                Config_mode_resets_confirmed_alarms: panelConfig.Config_mode_resets_confirmed_alarms,
                Config_mode_resets_tamper_alarms: panelConfig.Config_mode_resets_tamper_alarms,
                Config_mode_resets_low_battery_alarms: panelConfig.Config_mode_resets_low_battery_alarms,
                Config_mode_resets_superviz_alarms: panelConfig.Config_mode_resets_superviz_alarms,
                Cannot_arm_if_the_system_battery_is_low_or_AC_Fail: panelConfig.Cannot_arm_if_the_system_battery_is_low_or_AC_Fail,
                Cannot_arm_when_communication_fault: panelConfig.Cannot_arm_when_communication_fault,
                Limit_user_code_length: panelConfig.Limit_user_code_length,
                Enable_Outputs_Tamper: panelConfig.Enable_Outputs_Tamper,
                max_report_count: panelConfig.max_report_count,
                panel_title: panelConfig.panel_title,
                EN_Compliance: panelConfig.EN_Compliance,
                Code_Required_to_View_Memory: panelConfig.Code_Required_to_View_Memory,
                Cancel_Handover_Zone_Function_in_Stay_Mode: panelConfig.Cancel_Handover_Zone_Function_in_Stay_Mode,
                license_time: panelConfig.license_time
            }
        }
        else {
            return {};
        }
    }
}