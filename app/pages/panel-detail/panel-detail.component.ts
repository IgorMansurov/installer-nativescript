import { Component, ViewChild } from "@angular/core";
import { PanelConfigService } from "../../shared/panel/panel-config.service";
import { RouterService } from "../../shared/router.service";
import { PageRoute } from "nativescript-angular/router";
import { TranslateService } from 'ng2-translate';

@Component({
    moduleId: module.id,
    selector: "panel-detail",
    templateUrl: "./panel-detail.html",
    styleUrls: ["./panel-detail-common.css", "./panel-detail.css"],
    providers: []
})
export class PanelDetailComponent {
    public panelId: number;
    public title: string;
    public selectedTabIndex: number;
    public tabNames: any;
    public components: any;
    public tabName: string;

    @ViewChild('communication') communication;
    @ViewChild('miscellaneous') miscellaneous;

    constructor(private panelConfigService: PanelConfigService, private pageRoute: PageRoute, public routerService: RouterService, private translate: TranslateService) {
        this.pageRoute.activatedRoute
            .switchMap(activatedRoute => activatedRoute.params)
            .forEach((params) => {
                this.panelId = +params["panelId"];
                this.tabName = params["tabName"];
            });
    }

    ngOnInit() {
        this.components = new Array();
        this.components.push({id: "users", name: "Users"});
        this.components.push({id: "areas", name: "Areas"});
        this.components.push({id: "zones", name: "Zones"});
        this.components.push({id: "outputs", name: "Outputs"});
        this.components.push({id: "report-channels", name: "Report Channels"});
        this.components.push({id: "keypads", name: "Keypads"});
        this.components.push({id: "communication", name: "Communication"});
        this.components.push({id: "miscellaneous", name: "Miscellaneous"});
        this.components.push({id: "diagnostic", name: "Diagnostic"});

        //this.selectedTabIndex = this.tabNames.indexOf(this.tabName);

        this.translate.get('COMMON.PANEL').subscribe((res: string) => {
            this.title = res + ' ' + this.panelId;
        });
    }

    public onNavBackBtnTap() {
        return this.routerService.simpleBackward('/list');
    }

    public onIndexChanged(args) {
        //let tabView = <TabView>args.object;
        //this.selectedTabIndex = tabView.selectedIndex;
    }

    public saveCommunication() {
        this.communication.save();
    }

    public saveMiscellaneous() {
        this.miscellaneous.save();
    }

    public getTitle() {
        return this.title;
    }

    public navigateTo(component: string = 'users') {
        return this.routerService.simpleBackward('/panel-detail/' + this.panelId + '/' + component);
    }
}