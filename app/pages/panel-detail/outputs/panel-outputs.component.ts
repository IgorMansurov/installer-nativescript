import { Component, OnInit } from "@angular/core";
import { PageRoute } from "nativescript-angular/router";
import { PanelOutputsService } from "../../../shared/panel/panel-outputs.service";
import { RouterService } from "../../../shared/router.service";

@Component({
    moduleId: module.id,
    selector: "panel-outputs",
    templateUrl: "./panel-outputs.html",
    styleUrls: ["./panel-outputs-common.css", "./panel-outputs.css"],
    providers: []
})
export class PanelOutputsComponent implements OnInit {

    public outputs: any;
    public panelId: number;
    public title: string;

    constructor(private panelOutputsService: PanelOutputsService, private pageRoute: PageRoute, public routerService: RouterService) {
        this.routerService.isLoading = true;
        this.pageRoute.activatedRoute
            .switchMap(activatedRoute => activatedRoute.params)
            .forEach((params) => {
                this.panelId = +params["panelId"];
                this.title = "Panel " + this.panelId + ' > Outputs';
            });
    }

    ngOnInit() {
        this.outputs = this.panelOutputsService.getPanelOutputList(this.panelId);
    }

    public showAddOutputButton() {
        return this.panelOutputsService.quantityEmptyOutputs(this.panelId) > 0;
    }

    public editOutput(outputId: number = -1) {
        this.routerService.isLoading = true;
        if (outputId === -1) {
            outputId = this.panelOutputsService.getFirstEmptyOutput(this.panelId);
        }
        this.routerService.simpleForward('/panel-detail/' + this.panelId + '/outputs/' + outputId);
    }

    public onNavBackBtnTap() {
        this.routerService.isLoading = true;
        this.routerService.simpleBackward('/panel-detail/' + this.panelId);
    }
}