import { Component, OnInit, ViewChild } from "@angular/core";
import { PageRoute } from "nativescript-angular/router";
import { RouterService } from "../../../shared/router.service";
import { ReportChannelService } from "./report-channel.service";
import { Helper, PickType, PickValueConverter } from "./../../../shared/helper";
import { Config } from "../../../shared/config";
import { RadDataFormComponent } from "nativescript-pro-ui/dataform/angular";

@Component({
    moduleId: module.id,
    selector: "report-channel",
    templateUrl: "./report-channel.html",
    styleUrls: ["./report-channel-common.css", "./report-channel.css"],
    providers: []
})
export class ReportChannelComponent implements OnInit {

    public title: string;
    public panelId: number;
    public reportChannelId: number;

    public configReportChannel: any;
    public configReportChannelBackup: any;
    public reportChannelForDataForm: any;

    public channel_backup: any;
    public account_number: any;

    public labelTextSize: number;

    public reportChannelTypes: Array<PickType>;
    public reportChannelTypeConverter: PickValueConverter;
    public reportChannelTypeNames: Array<String>;
    public reportProtocolTypes: Array<PickType>;
    public reportProtocolTypeConverter: PickValueConverter;
    public reportProtocolTypeNames: Array<String>;

    public keys(object: any) {
        return Helper.keys(object);
    }

    constructor(private pageRoute: PageRoute, private reportChannelService: ReportChannelService, public routerService: RouterService) {
        this.pageRoute.activatedRoute
            .switchMap(activatedRoute => activatedRoute.params)
            .forEach((params) => {
                this.panelId = +params["panelId"];
                this.reportChannelId = +params["reportChannelId"];
                this.title = "Panel " + this.panelId + ' > Report Channels > ' + this.reportChannelService.getReportChannelNameById(this.panelId, this.reportChannelId);
            });
    }

    @ViewChild('backupChannelsForm') backupChannelsForm: RadDataFormComponent;

    ngOnInit() {
        this.reportChannelTypes = new Array<PickType>();
        for(let key in Config.channelTypes) {
            this.reportChannelTypes.push(new PickType(+key, Config.channelTypes[+key]));
        }
        this.reportChannelTypeConverter = new PickValueConverter(this.reportChannelTypes);
        this.reportChannelTypeNames = PickType.pickTypeNames(this.reportChannelTypes);

        this.reportProtocolTypes = new Array<PickType>();
        for(let key in Config.reportProtocolTypes) {
            this.reportProtocolTypes.push(new PickType(+key, Config.reportProtocolTypes[+key]));
        }
        this.reportProtocolTypeConverter = new PickValueConverter(this.reportProtocolTypes);
        this.reportProtocolTypeNames = PickType.pickTypeNames(this.reportProtocolTypes);

        this.configReportChannel = this.reportChannelService.getReportChannel(this.panelId, this.reportChannelId);
        if(this.configReportChannel) {
            this.configReportChannelBackup = this.configReportChannel;
            this.deComposeConfigReportChannel();
        }
        this.labelTextSize = 16;
    }

    private deComposeConfigReportChannel() {
        this.reportChannelForDataForm = Helper.deepCopyJsonObject(this.configReportChannel);
        delete this.reportChannelForDataForm.channel_backup;
        delete this.reportChannelForDataForm.account_number;

        this.channel_backup = {};
        this.account_number = {};

        for (let channelId in this.configReportChannel.channel_backup) {
            this.channel_backup[channelId] = this.configReportChannel.channel_backup[channelId]["assigned"];
        }
        for (let areaId in this.configReportChannel.account_number) {
            this.account_number[areaId] = this.configReportChannel.account_number[areaId]["number"];
        }
    }

    private composeConfigReportChannel() {
        let channel_backup = Helper.deepCopyJsonObject(this.configReportChannel.channel_backup);
        let account_number = Helper.deepCopyJsonObject(this.configReportChannel.account_number);
        this.configReportChannel = Helper.deepCopyJsonObject(this.reportChannelForDataForm);
        for (let channelId in this.channel_backup) {
            channel_backup[channelId]["assigned"] = this.channel_backup[channelId];
        }
        for (let areaId in this.account_number) {
            account_number[areaId]["number"] = this.account_number[areaId];
        }
        this.configReportChannel.channel_backup = Helper.deepCopyJsonObject(channel_backup);
        this.configReportChannel.account_number = Helper.deepCopyJsonObject(account_number);
    }

    public save() {
        this.routerService.isLoading = true;
        this.composeConfigReportChannel();
        this.reportChannelService.saveReportChannel(this.panelId, this.configReportChannel)
            .then(res => {
                this.routerService.simpleBackward('/panel-detail/' + this.panelId + '/report-channels');
            })
            .catch(error => {
                this.configReportChannel = this.configReportChannelBackup;
                this.reportChannelService.restoreReportChannel(this.panelId, this.configReportChannelBackup);
                alert(error);
                this.routerService.isLoading = false;
            });
    }

    public onNavBackBtnTap() {
        this.routerService.isLoading = true;
        this.routerService.simpleBackward('/panel-detail/' + this.panelId + '/report-channels');
    }

    public isEditableChannelType() {
        return this.reportChannelId !== Config.reservedChannelId;
    }

    public dfPropertyCommitted(args) {
        if( args.propertyName === 'active_channel' ) {
            Helper.reloadRadDataFormProperties(this,'backupChannelsForm', 'channel_backup', this.channel_backup);
        }
    }
}