import { Injectable } from "@angular/core";
import { PanelConfigService } from "../../../shared/panel/panel-config.service";
import { Helper } from "../../../shared/helper";
import { Config } from "../../../shared/config";

@Injectable()
export class ReportChannelService {

    constructor(private panelConfigService: PanelConfigService) {}

    public saveReportChannel(panelId: number, reportChannel: any) {
        this.saveReportChannelLocally(panelId, reportChannel);
        return this.panelConfigService.saveConfig();
    }
    public restoreReportChannel(panelId: number, reportChannel: any) {
        this.saveReportChannelLocally(panelId, reportChannel);
    }
    public getReportChannelNameById(panelId: number, reportChannelId: number) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        return Config.channelTypes[panelConfig.alt_flags_channel_type[reportChannelId]];
    }
    public saveReportChannelLocally(panelId: number, reportChannel: any) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        let keysToIgnore = ['id','channel_backup','failed_channel_restore_time','account_number'];
        for (let key in reportChannel) {
            if(keysToIgnore.indexOf(key) === -1) {
                panelConfig[key][reportChannel.id] = reportChannel[key];
            }
        }
        panelConfig['failed_channel_restore_time'] = reportChannel['failed_channel_restore_time'];
        for (let channelId in reportChannel.channel_backup) {
            panelConfig.channel_backup[channelId][reportChannel.id] = reportChannel.channel_backup[channelId]['assigned'];
        }
        for (let areaId in reportChannel.account_number) {
            panelConfig.account_number[areaId][reportChannel.id] = reportChannel.account_number[areaId]['number'];
        }
        this.panelConfigService.saveConfigLocally(panelConfig);
    }
    public getReportChannel(panelId: number, reportChannelId: number) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        if(panelConfig) {
            let channel_backup = {};
            let account_number = {};

            for(let k in panelConfig.channel_backup) {
                if(+k !== reportChannelId && +k !== Config.reservedChannelId) {
                    channel_backup[k] = {
                        name: Config.channelTypes[panelConfig.alt_flags_channel_type[k]] + ' (' + String(+k + 1) + ')',
                        assigned: panelConfig.channel_backup[k][reportChannelId]
                    };
                }
            }
            for(let areaId in panelConfig.area_name) {
                account_number[areaId] = {
                    name: 'Area ' + String(+areaId + 1) + ' account',
                    number: panelConfig.account_number[areaId][reportChannelId]
                };
            }
            return {
                id: reportChannelId,
                alt_flags_channel_type: panelConfig.alt_flags_channel_type[reportChannelId],
                active_channel: panelConfig.active_channel[reportChannelId],
                phone_number_or_server_address: panelConfig.phone_number_or_server_address[reportChannelId],
                report_protocol_type: panelConfig.report_protocol_type[reportChannelId],
                report_protocol_port: panelConfig.report_protocol_port[reportChannelId],
                channel_backup: channel_backup,
                failed_channel_restore_time: panelConfig.failed_channel_restore_time,
                account_number: account_number,
                report_video_event: panelConfig.report_video_event[reportChannelId],
                report_mains_failure: panelConfig.report_mains_failure[reportChannelId],
                report_system_battery_low: panelConfig.report_system_battery_low[reportChannelId],
                report_communication_fail: panelConfig.report_communication_fail[reportChannelId],
                report_system_tamper: panelConfig.report_system_tamper[reportChannelId],
                report_zone_tamper: panelConfig.report_zone_tamper[reportChannelId],
                report_duress_alarm: panelConfig.report_duress_alarm[reportChannelId],
                report_panic_alarm: panelConfig.report_panic_alarm[reportChannelId],
                report_manual_fire_alarm: panelConfig.report_manual_fire_alarm[reportChannelId],
                report_manual_medical_alarm: panelConfig.report_manual_medical_alarm[reportChannelId],
                report_zone_bypasses: panelConfig.report_zone_bypasses[reportChannelId],
                report_arm_disarm: panelConfig.report_arm_disarm[reportChannelId],
                report_stay_mode_arm_disarm: panelConfig.report_stay_mode_arm_disarm[reportChannelId],
                report_disarm_only_after_an_activation: panelConfig.report_disarm_only_after_an_activation[reportChannelId],
                report_stay_mode_disarm_only_after_an_activation: panelConfig.report_stay_mode_disarm_only_after_an_activation[reportChannelId],
                report_access_to_program_mode: panelConfig.report_access_to_program_mode[reportChannelId],
                report_zone_restores: panelConfig.report_zone_restores[reportChannelId],
                report_delinquent: panelConfig.report_delinquent[reportChannelId],
                report_fuse_failure: panelConfig.report_fuse_failure[reportChannelId],
                report_radio_battery_low: panelConfig.report_radio_battery_low[reportChannelId],
                report_supervised_radio_alarm: panelConfig.report_supervised_radio_alarm[reportChannelId],
                report_zone_sensor_watch_alarm: panelConfig.report_zone_sensor_watch_alarm[reportChannelId],
                report_latchkey_disarm: panelConfig.report_latchkey_disarm[reportChannelId],
                report_comm_interference_detected: panelConfig.report_comm_interference_detected[reportChannelId],
                report_output_fail: panelConfig.report_output_fail[reportChannelId],
                report_test_connections: panelConfig.report_test_connections[reportChannelId],
                report_stay_mode_zone_alarms: panelConfig.report_stay_mode_zone_alarms[reportChannelId],
                report_output_changed: panelConfig.report_output_changed[reportChannelId],
                report_peripheral_tamper: panelConfig.report_peripheral_tamper[reportChannelId],
                report_zone_confirmed_alarm: panelConfig.report_zone_confirmed_alarm[reportChannelId]
            }
        }
        else {
            return {};
        }

    }
}