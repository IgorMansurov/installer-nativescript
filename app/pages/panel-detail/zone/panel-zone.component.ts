import { Component, OnInit, ViewChild } from "@angular/core";
import { PageRoute } from "nativescript-angular/router";
import { RouterService } from "../../../shared/router.service";
import { PanelZoneService } from "./panel-zone.service";
import { Helper, ZoneMain, WorkingMode, WorkingModeConverter } from "./../../../shared/helper";

import { ButtonEditorHelper } from "../../../shared/button-helper";
import { RadDataFormComponent } from "nativescript-pro-ui/dataform/angular";
import { android as androidApplication } from "tns-core-modules/application";

import { BarcodeScanService } from "../../../shared/barcode-scan.service";

@Component({
    moduleId: module.id,
    selector: "panel-zone",
    templateUrl: "./panel-zone.html",
    styleUrls: ["./panel-zone-common.css", "./panel-zone.css"],
    providers: []
})

export class PanelZoneComponent implements OnInit {

    public title: string;
    private _panelConfigZone: any;
    public panelConfigZoneBackup: any;
    public panelId: number;
    public panelZoneId: number;
    public panelZone: any;
    public zoneMainProperties: any;
    public zone_assigned_to_area: any;
    public zone_alarm_assigned_to_output: any;
    public zone_stay_alarm_assigned_to_output: any;
    public zone_24_hour_alarm_assigned_to_output: any;
    public zone_tamper_assigned_to_output: any;
    public chime_zone_alarm_assigned_to_output: any;
    public armed_zone_entry_delay_assigned_to_output: any;
    public stay_mode_entry_delay_assigned_to_output: any;
    public zone_near_alarm_assigned_to_output: any;
    public zone_verified_alarm_assigned_to_output: any;


    public isLoading: boolean;
    public labelTextSize: number;

    private _workingModes: Array<WorkingMode>;
    private _workingModeNames: Array<String>;
    private _workingModeConverter: WorkingModeConverter;

    public buttonBarcode: any;
    private _buttonEditorHelper;


    get workingModes() {
        if (!this._workingModes) {
            this._workingModes = new Array<WorkingMode>();
            this._workingModes.push(new WorkingMode(1, "Normal"));
            this._workingModes.push(new WorkingMode(2, "24 hour"));
            this._workingModes.push(new WorkingMode(4, "24 hour auto-reset"));
            this._workingModes.push(new WorkingMode(8, "24 hour fire zone"));
            this._workingModes.push(new WorkingMode(10, "Chime"));
            this._workingModes.push(new WorkingMode(20, "Permanent chime"));
        }
        return this._workingModes;
    }

    get workingModeNames() {
        if (!this._workingModeNames) {
            this._workingModeNames = this.workingModes.map((value: WorkingMode) => value.name);
        }
        return this._workingModeNames;
    }

    get workingModeConverter() {
        return this._workingModeConverter;
    }

    get panelConfigZone() {
        return this._panelConfigZone;
    }

    set panelConfigZone(v: any) {
        this._panelConfigZone = v;
    }

    public keys(object: any) {
        return Helper.keys(object);
    }

    constructor(private pageRoute: PageRoute, private panelZoneService: PanelZoneService, public routerService: RouterService, private barcodeScanService: BarcodeScanService) {
        this.pageRoute.activatedRoute
            .switchMap(activatedRoute => activatedRoute.params)
            .forEach((params) => {
                this.panelId = +params["panelId"];
                this.panelZoneId = +params["zoneId"];
                this.title = "Panel " + this.panelId + ' > Zones > ' + this.panelZoneService.getZoneNameById(this.panelId, this.panelZoneId);
            });
    }

    ngOnInit() {
        this._workingModeConverter = new WorkingModeConverter(this.workingModes);
        this.panelConfigZone = this.panelZoneService.getZone(this.panelId, this.panelZoneId);
        if (this.panelConfigZone) {
            this.panelConfigZoneBackup = this.panelConfigZone;
            this._deComposeConfigZone();
        }
        else {
            this.panelZone = {};
        }
        this.labelTextSize = 16;
        this.isLoading = false;
    }

    @ViewChild('zoneMainPropertiesForm') zoneMainPropertiesForm: RadDataFormComponent;

    private _deComposeConfigZone() {
        this.panelZone = Helper.deepCopyJsonObject(this.panelConfigZone);
        delete this.panelZone.zone_name;
        delete this.panelZone.radio_zones_list;
        delete this.panelZone.zone_assigned_to_area;
        delete this.panelZone.zone_alarm_assigned_to_output;
        delete this.panelZone.zone_stay_alarm_assigned_to_output;
        delete this.panelZone.zone_24_hour_alarm_assigned_to_output;
        delete this.panelZone.zone_tamper_assigned_to_output;
        delete this.panelZone.chime_zone_alarm_assigned_to_output;
        delete this.panelZone.armed_zone_entry_delay_assigned_to_output;
        delete this.panelZone.stay_mode_entry_delay_assigned_to_output;
        delete this.panelZone.zone_near_alarm_assigned_to_output;
        delete this.panelZone.zone_verified_alarm_assigned_to_output;

        this.zone_assigned_to_area = {};
        this.zone_alarm_assigned_to_output = {};
        this.zone_stay_alarm_assigned_to_output = {};
        this.zone_24_hour_alarm_assigned_to_output = {};
        this.zone_tamper_assigned_to_output = {};
        this.chime_zone_alarm_assigned_to_output = {};
        this.armed_zone_entry_delay_assigned_to_output = {};
        this.stay_mode_entry_delay_assigned_to_output = {};
        this.zone_near_alarm_assigned_to_output = {};
        this.zone_verified_alarm_assigned_to_output = {};

        for (let areaId in this.panelConfigZone.zone_assigned_to_area) {
            this.zone_assigned_to_area[areaId] = this.panelConfigZone.zone_assigned_to_area[areaId]["assigned"];
        }

        for (let outputId in this.panelConfigZone.zone_alarm_assigned_to_output) {
            this.zone_alarm_assigned_to_output[outputId] = this.panelConfigZone.zone_alarm_assigned_to_output[outputId]["assigned"];
            this.zone_stay_alarm_assigned_to_output[outputId] = this.panelConfigZone.zone_stay_alarm_assigned_to_output[outputId]["assigned"];
            this.zone_24_hour_alarm_assigned_to_output[outputId] = this.panelConfigZone.zone_24_hour_alarm_assigned_to_output[outputId]["assigned"];
            this.zone_tamper_assigned_to_output[outputId] = this.panelConfigZone.zone_tamper_assigned_to_output[outputId]["assigned"];
            this.chime_zone_alarm_assigned_to_output[outputId] = this.panelConfigZone.chime_zone_alarm_assigned_to_output[outputId]["assigned"];
            this.armed_zone_entry_delay_assigned_to_output[outputId] = this.panelConfigZone.armed_zone_entry_delay_assigned_to_output[outputId]["assigned"];
            this.stay_mode_entry_delay_assigned_to_output[outputId] = this.panelConfigZone.stay_mode_entry_delay_assigned_to_output[outputId]["assigned"];
            this.zone_near_alarm_assigned_to_output[outputId] = this.panelConfigZone.zone_near_alarm_assigned_to_output[outputId]["assigned"];
            this.zone_verified_alarm_assigned_to_output[outputId] = this.panelConfigZone.zone_verified_alarm_assigned_to_output[outputId]["assigned"];
        }

        this.zoneMainProperties = new ZoneMain();
        this.zoneMainProperties.zone_name = this.panelConfigZone.zone_name;
        this.zoneMainProperties.radio_zones_list = this.panelConfigZone.radio_zones_list;
        this.zoneMainProperties.buttonBarcode = this.zoneMainProperties.radio_zones_list;
    }

    private _composeConfigZone() {
        let zone_assigned_to_area = Helper.deepCopyJsonObject(this.panelConfigZone.zone_assigned_to_area);
        let zone_alarm_assigned_to_output = Helper.deepCopyJsonObject(this.panelConfigZone.zone_alarm_assigned_to_output);
        let zone_stay_alarm_assigned_to_output = Helper.deepCopyJsonObject(this.panelConfigZone.zone_stay_alarm_assigned_to_output);
        let zone_24_hour_alarm_assigned_to_output = Helper.deepCopyJsonObject(this.panelConfigZone.zone_24_hour_alarm_assigned_to_output);
        let zone_tamper_assigned_to_output = Helper.deepCopyJsonObject(this.panelConfigZone.zone_tamper_assigned_to_output);
        let chime_zone_alarm_assigned_to_output = Helper.deepCopyJsonObject(this.panelConfigZone.chime_zone_alarm_assigned_to_output);
        let armed_zone_entry_delay_assigned_to_output = Helper.deepCopyJsonObject(this.panelConfigZone.armed_zone_entry_delay_assigned_to_output);
        let stay_mode_entry_delay_assigned_to_output = Helper.deepCopyJsonObject(this.panelConfigZone.stay_mode_entry_delay_assigned_to_output);
        let zone_near_alarm_assigned_to_output = Helper.deepCopyJsonObject(this.panelConfigZone.zone_near_alarm_assigned_to_output);
        let zone_verified_alarm_assigned_to_output = Helper.deepCopyJsonObject(this.panelConfigZone.zone_verified_alarm_assigned_to_output);

        this.panelConfigZone = Helper.deepCopyJsonObject(this.panelZone);

        for (let areaId in this.zone_assigned_to_area) {
            zone_assigned_to_area[areaId]["assigned"] = this.zone_assigned_to_area[areaId];
        }
        for (let outputId in this.zone_alarm_assigned_to_output) {
            zone_alarm_assigned_to_output[outputId]["assigned"] = this.zone_alarm_assigned_to_output[outputId];
            zone_stay_alarm_assigned_to_output[outputId]["assigned"] = this.zone_stay_alarm_assigned_to_output[outputId];
            zone_24_hour_alarm_assigned_to_output[outputId]["assigned"] = this.zone_24_hour_alarm_assigned_to_output[outputId];
            zone_tamper_assigned_to_output[outputId]["assigned"] = this.zone_tamper_assigned_to_output[outputId];
            chime_zone_alarm_assigned_to_output[outputId]["assigned"] = this.chime_zone_alarm_assigned_to_output[outputId];
            armed_zone_entry_delay_assigned_to_output[outputId]["assigned"] = this.armed_zone_entry_delay_assigned_to_output[outputId];
            stay_mode_entry_delay_assigned_to_output[outputId]["assigned"] = this.stay_mode_entry_delay_assigned_to_output[outputId];
            zone_near_alarm_assigned_to_output[outputId]["assigned"] = this.zone_near_alarm_assigned_to_output[outputId];
            zone_verified_alarm_assigned_to_output[outputId]["assigned"] = this.zone_verified_alarm_assigned_to_output[outputId];
        }

        delete this.zoneMainProperties.buttonBarcode;
        this.panelConfigZone.zone_name = this.zoneMainProperties.zone_name;
        this.panelConfigZone.radio_zones_list = this.zoneMainProperties.radio_zones_list;
        this.panelConfigZone.zone_assigned_to_area = Helper.deepCopyJsonObject(zone_assigned_to_area);
        this.panelConfigZone.zone_alarm_assigned_to_output = Helper.deepCopyJsonObject(zone_alarm_assigned_to_output);
        this.panelConfigZone.zone_stay_alarm_assigned_to_output = Helper.deepCopyJsonObject(zone_stay_alarm_assigned_to_output);
        this.panelConfigZone.zone_24_hour_alarm_assigned_to_output = Helper.deepCopyJsonObject(zone_24_hour_alarm_assigned_to_output);
        this.panelConfigZone.zone_tamper_assigned_to_output = Helper.deepCopyJsonObject(zone_tamper_assigned_to_output);
        this.panelConfigZone.chime_zone_alarm_assigned_to_output = Helper.deepCopyJsonObject(chime_zone_alarm_assigned_to_output);
        this.panelConfigZone.armed_zone_entry_delay_assigned_to_output = Helper.deepCopyJsonObject(armed_zone_entry_delay_assigned_to_output);
        this.panelConfigZone.stay_mode_entry_delay_assigned_to_output = Helper.deepCopyJsonObject(stay_mode_entry_delay_assigned_to_output);
        this.panelConfigZone.zone_near_alarm_assigned_to_output = Helper.deepCopyJsonObject(zone_near_alarm_assigned_to_output);
        this.panelConfigZone.zone_verified_alarm_assigned_to_output = Helper.deepCopyJsonObject(zone_verified_alarm_assigned_to_output);
    }

    public save() {
        this.routerService.isLoading = true;
        this.zoneMainPropertiesForm.dataForm.commitAll();
        this._composeConfigZone();
        this.panelZoneService.saveZone(this.panelId, this.panelConfigZone)
            .then(res => {
                this.routerService.simpleBackward('/panel-detail/' + this.panelId + '/zones');
            })
            .catch(error => {
                this.panelConfigZone = this.panelConfigZoneBackup;
                this.panelZoneService.restoreZone(this.panelId, this.panelConfigZoneBackup);
                alert(error);
                this.routerService.isLoading = false;
            });
    }

    public onNavBackBtnTap() {
        this.routerService.isLoading = true;
        this.routerService.simpleBackward('/panel-detail/' + this.panelId + '/zones');
    }

    public editorNeedsView(args) {
        if (androidApplication) {
            this._buttonEditorHelper = new ButtonEditorHelper();
            this._buttonEditorHelper.editor = args.object;
            let androidEditorView: android.widget.Button = new android.widget.Button(args.context);
            let that = this;
            androidEditorView.setOnClickListener(new android.view.View.OnClickListener({
                onClick(view: android.view.View) {
                    that.handleTap(view, args.object);
                }
            }));
            args.view = androidEditorView;
            this.updateEditorValue(androidEditorView, this.zoneMainProperties.radio_zones_list);
        } else {
            this._buttonEditorHelper = new ButtonEditorHelper();
            this._buttonEditorHelper.editor = args.object;
            let iosEditorView = UIButton.buttonWithType(UIButtonType.System);
            iosEditorView.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left;
            iosEditorView.addTargetActionForControlEvents(this._buttonEditorHelper, "handleTap:", UIControlEvents.TouchUpInside);
            args.view = iosEditorView;
        }
    }

    public editorHasToApplyValue(args) {
        this._buttonEditorHelper.updateEditorValue(args.view, args.value);
    }

    public editorNeedsValue(args) {
        args.value = this._buttonEditorHelper.buttonValue;
    }

    public updateEditorValue(editorView, value) {
        this._buttonEditorHelper.buttonValue = value;
        editorView.setText("Tap to scan zone's barcode");
    }

    public handleTap(editorView, editor) {
        this.barcodeScanService.scanBarcode()
            .then((result) => {
                    this.updateEditorValue(editorView, result.text);
                    editor.notifyValueChanged();
                    /* TODO
                    https://github.com/telerik/nativescript-ui-feedback/issues/344
                    * */
                    Helper.reloadRadDataFormProperties(this, 'zoneMainPropertiesForm', 'zoneMainProperties', {
                        "radio_zones_list": +result.text
                    });
                }, (errorMessage) => {
                    alert("No scan. " + errorMessage);
                    console.log("No scan. " + errorMessage);
                }
            )
            .catch((error) => alert('Error scan barcode.. ' + error));
    }
}