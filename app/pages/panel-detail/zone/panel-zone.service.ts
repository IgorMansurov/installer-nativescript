import { Injectable } from "@angular/core";
import {PanelConfigService} from "../../../shared/panel/panel-config.service";
import {Helper} from "../../../shared/helper";

@Injectable()
export class PanelZoneService {

    constructor(private panelConfigService: PanelConfigService) {}

    public saveZone(panelId: number, zone: any) {
        this.saveZoneLocally(panelId, zone);
        return this.panelConfigService.saveConfig();
    }
    public restoreZone(panelId: number, zone: any) {
        this.saveZoneLocally(panelId, zone);
    }
    public getZoneNameById(panelId: number, zoneId: number) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        return panelConfig.zone_name[zoneId];
    }
    public saveZoneLocally(panelId: number, zone: any) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        let keysToIgnore = ['id', 'zone_assigned_to_area', 'zone_alarm_assigned_to_output', 'zone_stay_alarm_assigned_to_output', 'zone_24_hour_alarm_assigned_to_output',
                            'zone_tamper_assigned_to_output', 'chime_zone_alarm_assigned_to_output', 'armed_zone_entry_delay_assigned_to_output', 'stay_mode_entry_delay_assigned_to_output',
                            'zone_near_alarm_assigned_to_output', 'zone_verified_alarm_assigned_to_output'];

        for (let key in zone) {
            if(keysToIgnore.indexOf(key) === -1) {
                panelConfig[key][zone.id] = zone[key];
            }
        }
        for (let areaId in zone.zone_assigned_to_area) {
            panelConfig.zone_assigned_to_area[areaId][zone.id] = zone.zone_assigned_to_area[areaId]['assigned'];
        }
        for (let outputId in zone.zone_alarm_assigned_to_output) {
            panelConfig.zone_alarm_assigned_to_output[outputId][zone.id] = zone.zone_alarm_assigned_to_output[outputId]['assigned'];
            panelConfig.zone_stay_alarm_assigned_to_output[outputId][zone.id] = zone.zone_stay_alarm_assigned_to_output[outputId]['assigned'];
            panelConfig.zone_24_hour_alarm_assigned_to_output[outputId][zone.id] = zone.zone_24_hour_alarm_assigned_to_output[outputId]['assigned'];
            panelConfig.zone_tamper_assigned_to_output[outputId][zone.id] = zone.zone_tamper_assigned_to_output[outputId]['assigned'];
            panelConfig.chime_zone_alarm_assigned_to_output[outputId][zone.id] = zone.chime_zone_alarm_assigned_to_output[outputId]['assigned'];
            panelConfig.armed_zone_entry_delay_assigned_to_output[outputId][zone.id] = zone.armed_zone_entry_delay_assigned_to_output[outputId]['assigned'];
            panelConfig.stay_mode_entry_delay_assigned_to_output[outputId][zone.id] = zone.stay_mode_entry_delay_assigned_to_output[outputId]['assigned'];
            panelConfig.zone_near_alarm_assigned_to_output[outputId][zone.id] = zone.zone_near_alarm_assigned_to_output[outputId]['assigned'];
            panelConfig.zone_verified_alarm_assigned_to_output[outputId][zone.id] = zone.zone_verified_alarm_assigned_to_output[outputId]['assigned'];
        }
        this.panelConfigService.saveConfigLocally(panelConfig);
    }


    public getZone(panelId: number, zoneId: number) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        if(panelConfig) {
            let zone_assigned_to_area = {};
            let zone_alarm_assigned_to_output = {};
            let zone_stay_alarm_assigned_to_output = {};
            let zone_24_hour_alarm_assigned_to_output = {};
            let zone_tamper_assigned_to_output = {};
            let chime_zone_alarm_assigned_to_output = {};
            let armed_zone_entry_delay_assigned_to_output = {};
            let stay_mode_entry_delay_assigned_to_output = {};
            let zone_near_alarm_assigned_to_output = {};
            let zone_verified_alarm_assigned_to_output = {};


            for (let areaId in panelConfig.zone_assigned_to_area) {
                zone_assigned_to_area[areaId] = {
                    name: panelConfig.area_name[areaId],
                    assigned: panelConfig.zone_assigned_to_area[areaId][zoneId]
                };
            }

            panelConfig.radio_outputs_list.map((element, outputId, array) => {
                if (!Helper.isEmpty(element)) {
                    zone_alarm_assigned_to_output[outputId] = {
                        name: panelConfig.output_name[outputId],
                        assigned: panelConfig.zone_alarm_assigned_to_output[outputId][zoneId]
                    };
                    zone_stay_alarm_assigned_to_output[outputId] = {
                        name: panelConfig.output_name[outputId],
                        assigned: panelConfig.zone_stay_alarm_assigned_to_output[outputId][zoneId]
                    };
                    zone_24_hour_alarm_assigned_to_output[outputId] = {
                        name: panelConfig.output_name[outputId],
                        assigned: panelConfig.zone_24_hour_alarm_assigned_to_output[outputId][zoneId]
                    };
                    zone_tamper_assigned_to_output[outputId] = {
                        name: panelConfig.output_name[outputId],
                        assigned: panelConfig.zone_tamper_assigned_to_output[outputId][zoneId]
                    };
                    chime_zone_alarm_assigned_to_output[outputId] = {
                        name: panelConfig.output_name[outputId],
                        assigned: panelConfig.chime_zone_alarm_assigned_to_output[outputId][zoneId]
                    };
                    armed_zone_entry_delay_assigned_to_output[outputId] = {
                        name: panelConfig.output_name[outputId],
                        assigned: panelConfig.armed_zone_entry_delay_assigned_to_output[outputId][zoneId]
                    };
                    stay_mode_entry_delay_assigned_to_output[outputId] = {
                        name: panelConfig.output_name[outputId],
                        assigned: panelConfig.stay_mode_entry_delay_assigned_to_output[outputId][zoneId]
                    };
                    zone_near_alarm_assigned_to_output[outputId] = {
                        name: panelConfig.output_name[outputId],
                        assigned: panelConfig.zone_near_alarm_assigned_to_output[outputId][zoneId]
                    };
                    zone_verified_alarm_assigned_to_output[outputId] = {
                        name: panelConfig.output_name[outputId],
                        assigned: panelConfig.zone_verified_alarm_assigned_to_output[outputId][zoneId]
                    };
                }
            });

            return {
                id: zoneId,
                zone_name: panelConfig.zone_name[zoneId],
                radio_zones_list: panelConfig.radio_zones_list[zoneId],
                zone_is_active: panelConfig.zone_is_active[zoneId],
                stay_mode_zone: panelConfig.stay_mode_zone[zoneId],
                two_trigger_zone: panelConfig.two_trigger_zone[zoneId],
                exit_delay_zone: panelConfig.exit_delay_zone[zoneId],
                low_security_zone: panelConfig.low_security_zone[zoneId],
                handover_zone: panelConfig.handover_zone[zoneId],
                manually_bypassed_zone: panelConfig.manually_bypassed_zone[zoneId],
                auto_bypassed_zone: panelConfig.auto_bypassed_zone[zoneId],
                alt_flags_working_mode: panelConfig.alt_flags_working_mode[zoneId],
                zone_will_not_report_24_hour_alarm: panelConfig.zone_will_not_report_24_hour_alarm[zoneId],
                zone_in_bypass_group: panelConfig.zone_in_bypass_group[zoneId],
                zone_sends_reports: panelConfig.zone_sends_reports[zoneId],
                zone_is_on_soak_test: panelConfig.zone_is_on_soak_test[zoneId],
                exit_terminator_zone: panelConfig.exit_terminator_zone[zoneId],
                zone_assigned_to_area: zone_assigned_to_area,
                armed_zone_entry_delay_time: panelConfig.armed_zone_entry_delay_time[zoneId],
                stay_mode_entry_delay_time: panelConfig.stay_mode_entry_delay_time[zoneId],
                sensor_inactivity_time: panelConfig.sensor_inactivity_time[zoneId],
                zone_re_trigger_count: panelConfig.zone_re_trigger_count[zoneId],
                zone_alarm_assigned_to_output: zone_alarm_assigned_to_output,
                zone_stay_alarm_assigned_to_output: zone_stay_alarm_assigned_to_output,
                zone_24_hour_alarm_assigned_to_output: zone_24_hour_alarm_assigned_to_output,
                zone_tamper_assigned_to_output: zone_tamper_assigned_to_output,
                chime_zone_alarm_assigned_to_output: chime_zone_alarm_assigned_to_output,
                armed_zone_entry_delay_assigned_to_output: armed_zone_entry_delay_assigned_to_output,
                stay_mode_entry_delay_assigned_to_output: stay_mode_entry_delay_assigned_to_output,
                zone_near_alarm_assigned_to_output: zone_near_alarm_assigned_to_output,
                zone_verified_alarm_assigned_to_output: zone_verified_alarm_assigned_to_output

            }
        }
        else {
            return {};
        }

    }
}