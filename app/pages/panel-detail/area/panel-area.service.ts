import {Injectable} from "@angular/core";
import {PanelConfigService} from "../../../shared/panel/panel-config.service";
import {Helper} from "../../../shared/helper";


@Injectable()
export class PanelAreaService {

    constructor(private panelConfigService: PanelConfigService) {
    }

    public saveArea(panelId: number, area: any) {
        this.saveAreaLocally(panelId, area);
        return this.panelConfigService.saveConfig();
    }

    public restoreArea(panelId: number,area: any) {
        this.saveAreaLocally(panelId, area);
    }

    public getAreaNameById(panelId: number, index: number) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        return panelConfig.area_name[index];
    }
    public saveAreaLocally(panelId: number, area: any) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        let keysToIgnore = ['id','area_zones','area_users', 'arm_indication_to_output','stay_arm_indication_to_output',
                            'disarm_indication_to_output','arm_beeps_to_output','armed_exit_delay_beeps_to_output','stay_exit_delay_beeps_to_output','stay_arm_beeps_to_output',
                            'pendant_tag_arm_beep_to_output','pendant_tag_disarm_beep_to_output','pendant_tag_stay_arm_beep_to_output',
                            'pendant_tag_stay_disarm_beep_to_output','disarm_beeps_to_output','stay_disarm_beeps_to_output','pulse_output_every_5_sec_when_disarmed'];
        for (let key in area) {
            if(keysToIgnore.indexOf(key) === -1) {
                panelConfig[key][area.id] = area[key];
            }
        }
        for (let k in area.area_zones) {
            panelConfig.zone_assigned_to_area[area.id][k] = area.area_zones[k]['assigned'];
        }
        for (let k in area.area_users) {
            panelConfig.user_assigned_to_area[area.id][k] = area.area_users[k]['assigned'];
        }
        panelConfig.radio_outputs_list.map((element, outputId, array) => {
            if (!Helper.isEmpty(element)) {
                panelConfig.arm_indication_to_output[outputId][area.id] = area.arm_indication_to_output[outputId]['assigned'];
                panelConfig.stay_arm_indication_to_output[outputId][area.id] = area.stay_arm_indication_to_output[outputId]['assigned'];
                panelConfig.disarm_indication_to_output[outputId][area.id] = area.disarm_indication_to_output[outputId]['assigned'];
                panelConfig.arm_beeps_to_output[outputId][area.id] = area.arm_beeps_to_output[outputId]['assigned'];
                panelConfig.armed_exit_delay_beeps_to_output[outputId][area.id] = area.armed_exit_delay_beeps_to_output[outputId]['assigned'];
                panelConfig.stay_exit_delay_beeps_to_output[outputId][area.id] = area.stay_exit_delay_beeps_to_output[outputId]['assigned'];
                panelConfig.stay_arm_beeps_to_output[outputId][area.id] = area.stay_arm_beeps_to_output[outputId]['assigned'];
                panelConfig.pendant_tag_arm_beep_to_output[outputId][area.id] = area.pendant_tag_arm_beep_to_output[outputId]['assigned'];
                panelConfig.pendant_tag_disarm_beep_to_output[outputId][area.id] = area.pendant_tag_disarm_beep_to_output[outputId]['assigned'];
                panelConfig.pendant_tag_stay_arm_beep_to_output[outputId][area.id] = area.pendant_tag_stay_arm_beep_to_output[outputId]['assigned'];
                panelConfig.pendant_tag_stay_disarm_beep_to_output[outputId][area.id] = area.pendant_tag_stay_disarm_beep_to_output[outputId]['assigned'];
                panelConfig.disarm_beeps_to_output[outputId][area.id] = area.disarm_beeps_to_output[outputId]['assigned'];
                panelConfig.stay_disarm_beeps_to_output[outputId][area.id] = area.stay_disarm_beeps_to_output[outputId]['assigned'];
                panelConfig.pulse_output_every_5_sec_when_disarmed[outputId][area.id] = area.pulse_output_every_5_sec_when_disarmed[outputId]['assigned'];
            }
        });
        this.panelConfigService.saveConfigLocally(panelConfig);
    }

    public getArea(panelId: number, areaId: number) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        if(panelConfig) {
            let areaZones = {};
            let areaUsers = {};
            let armIndicationToOutput = {};
            let stayArmIndicationToOutput = {};
            let disarmIndicationToOutput = {};
            let armBeepsToOutput = {};
            let armedExitDelayBeepsToOutput = {};
            let stayExitDelayBeepsToOutput = {};
            let stayArmBeepsToOutput = {};
            let pendantTagArmBeepToOutput = {};
            let pendantTagDisarmBeepToOutput = {};
            let pendantTagStayArmBeepToOutput = {};
            let pendantTagStayDisarmBeepToOutput = {};
            let disarmBeepsToOutput = {};
            let stayDisarmBeepsToOutput = {};
            let pulseOutputEvery5SecWhenDisarmed = {};

            for (let zoneId in panelConfig.zone_assigned_to_area[areaId]) {
                if (!Helper.isEmpty(panelConfig.radio_zones_list[zoneId])) {
                    areaZones[zoneId] = {
                        name: panelConfig.zone_name[zoneId],
                        assigned: panelConfig.zone_assigned_to_area[areaId][zoneId]
                    };
                }
            }
            for (let userId in panelConfig.user_assigned_to_area[areaId]) {
                if (!Helper.isEmpty(panelConfig.user_code[userId])) {
                    areaUsers[userId] = {
                        name: panelConfig.user_name[userId],
                        assigned: panelConfig.user_assigned_to_area[areaId][userId]
                    };
                }
            }
            panelConfig.radio_outputs_list.map((element, outputId, array) => {
                if (!Helper.isEmpty(element)) {
                    armIndicationToOutput[outputId] = {
                        name: panelConfig.output_name[outputId],
                        assigned: panelConfig.arm_indication_to_output[outputId][areaId]
                    };
                    stayArmIndicationToOutput[outputId] = {
                        name: panelConfig.output_name[outputId],
                        assigned: panelConfig.stay_arm_indication_to_output[outputId][areaId]
                    };
                    disarmIndicationToOutput[outputId] = {
                        name: panelConfig.output_name[outputId],
                        assigned: panelConfig.disarm_indication_to_output[outputId][areaId]
                    };
                    armBeepsToOutput[outputId] = {
                        name: panelConfig.output_name[outputId],
                        assigned: panelConfig.arm_beeps_to_output[outputId][areaId]
                    };
                    armedExitDelayBeepsToOutput[outputId] = {
                        name: panelConfig.output_name[outputId],
                        assigned: panelConfig.armed_exit_delay_beeps_to_output[outputId][areaId]
                    };
                    stayExitDelayBeepsToOutput[outputId] = {
                        name: panelConfig.output_name[outputId],
                        assigned: panelConfig.stay_exit_delay_beeps_to_output[outputId][areaId]
                    };
                    pendantTagArmBeepToOutput[outputId] = {
                        name: panelConfig.output_name[outputId],
                        assigned: panelConfig.pendant_tag_arm_beep_to_output[outputId][areaId]
                    };
                    pendantTagDisarmBeepToOutput[outputId] = {
                        name: panelConfig.output_name[outputId],
                        assigned: panelConfig.pendant_tag_disarm_beep_to_output[outputId][areaId]
                    };
                    pendantTagStayArmBeepToOutput[outputId] = {
                        name: panelConfig.output_name[outputId],
                        assigned: panelConfig.pendant_tag_stay_arm_beep_to_output[outputId][areaId]
                    };
                    pendantTagStayDisarmBeepToOutput[outputId] = {
                        name: panelConfig.output_name[outputId],
                        assigned: panelConfig.pendant_tag_stay_disarm_beep_to_output[outputId][areaId]
                    };
                    stayArmBeepsToOutput[outputId] = {
                        name: panelConfig.output_name[outputId],
                        assigned: panelConfig.stay_arm_beeps_to_output[outputId][areaId]
                    };
                    disarmBeepsToOutput[outputId] = {
                        name: panelConfig.output_name[outputId],
                        assigned: panelConfig.disarm_beeps_to_output[outputId][areaId]
                    };
                    stayDisarmBeepsToOutput[outputId] = {
                        name: panelConfig.output_name[outputId],
                        assigned: panelConfig.stay_disarm_beeps_to_output[outputId][areaId]
                    };
                    pulseOutputEvery5SecWhenDisarmed[outputId] = {
                        name: panelConfig.output_name[outputId],
                        assigned: panelConfig.pulse_output_every_5_sec_when_disarmed[outputId][areaId]
                    };
                }
            });
            return {
                id: areaId,
                area_name: panelConfig.area_name[areaId],
                code_required_to_bypass_zones: panelConfig.code_required_to_bypass_zones[areaId],
                code_required_to_set: panelConfig.code_required_to_set[areaId],
                arm_command_required_before_code_to_set: panelConfig.arm_command_required_before_code_to_set[areaId],
                stay_command_required_before_code_to_set_stay_mode: panelConfig.stay_command_required_before_code_to_set_stay_mode[areaId],
                report_arm_on_exit_delay: panelConfig.report_arm_on_exit_delay[areaId],
                use_near_and_verified_alarm_reporting_for_all_zones: panelConfig.use_near_and_verified_alarm_reporting_for_all_zones[areaId],
                unable_to_arm_if_exit_zone_is_open: panelConfig.unable_to_arm_if_exit_zone_is_open[areaId],
                exit_delay_time: panelConfig.exit_delay_time[areaId],
                stay_exit_delay_time: panelConfig.stay_exit_delay_time[areaId],
                delinquency_delay: panelConfig.delinquency_delay[areaId],
                area_inactive_time: panelConfig.area_inactive_time[areaId],
                area_zones: areaZones,
                area_users: areaUsers,
                arm_indication_to_output: armIndicationToOutput,
                stay_arm_indication_to_output: stayArmIndicationToOutput,
                disarm_indication_to_output: disarmIndicationToOutput,
                arm_beeps_to_output: armBeepsToOutput,
                armed_exit_delay_beeps_to_output: armedExitDelayBeepsToOutput,
                stay_exit_delay_beeps_to_output: stayExitDelayBeepsToOutput,
                pendant_tag_arm_beep_to_output: pendantTagArmBeepToOutput,
                pendant_tag_disarm_beep_to_output: pendantTagDisarmBeepToOutput,
                pendant_tag_stay_arm_beep_to_output: pendantTagStayArmBeepToOutput,
                pendant_tag_stay_disarm_beep_to_output: pendantTagStayDisarmBeepToOutput,
                stay_arm_beeps_to_output: stayArmBeepsToOutput,
                disarm_beeps_to_output: disarmBeepsToOutput,
                stay_disarm_beeps_to_output: stayDisarmBeepsToOutput,
                pulse_output_every_5_sec_when_disarmed: pulseOutputEvery5SecWhenDisarmed
            }
        }
        else {
            return {};
        }
    }
}