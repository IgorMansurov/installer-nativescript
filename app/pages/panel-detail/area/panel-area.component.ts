import { Component, OnInit } from "@angular/core";
import { PageRoute } from "nativescript-angular/router";
import { RouterService } from "../../../shared/router.service";
import { PanelAreaService } from "./panel-area.service";
import { Helper } from "./../../../shared/helper";

@Component({
    moduleId: module.id,
    selector: "panel-area",
    templateUrl: "./panel-area.html",
    styleUrls: ["./panel-area-common.css", "./panel-area.css"],
    providers: []
})

export class PanelAreaComponent implements OnInit {

    public title: string;
    private _panelConfigArea: any;
    public panelConfigAreaBackup: any;
    public panelId: number;
    public panelAreaId: number;
    public panelArea: any;
    public areaZones: any;
    public areaUsers: any;
    public armIndicationToOutput: any;
    public stayArmIndicationToOutput: any;
    public disarmIndicationToOutput: any;
    public armBeepsToOutput: any;
    public armedExitDelayBeepsToOutput: any;
    public stayExitDelayBeepsToOutput: any;
    public stayArmBeepsToOutput: any;
    public pendantTagArmBeepToOutput: any;
    public pendantTagDisarmBeepToOutput: any;
    public pendantTagStayArmBeepToOutput: any;
    public pendantTagStayDisarmBeepToOutput: any;
    public disarmBeepsToOutput: any;
    public stayDisarmBeepsToOutput: any;
    public pulseOutputEvery5SecWhenDisarmed: any;
    public labelTextSize: number;

    get panelConfigArea() {
        return this._panelConfigArea;
    }

    set panelConfigArea(v: any) {
        this._panelConfigArea = v;
    }

    public keys(object: any) {
        return Helper.keys(object);
    }

    constructor(private pageRoute: PageRoute, private panelAreaService: PanelAreaService, public routerService: RouterService) {
        this.pageRoute.activatedRoute
            .switchMap(activatedRoute => activatedRoute.params)
            .forEach((params) => {
                this.panelId = +params["panelId"];
                this.panelAreaId = +params["areaId"];
                this.title = "Panel " + this.panelId + ' > Areas > ' + this.panelAreaService.getAreaNameById(this.panelId, this.panelAreaId);
            });
    }

    ngOnInit() {
        this.panelConfigArea = this.panelAreaService.getArea(this.panelId, this.panelAreaId);
        this.panelConfigAreaBackup = this.panelConfigArea;
        this._deComposeConfigArea();
        this.labelTextSize = 16;
    }

    private _deComposeConfigArea() {
        this.panelArea = Helper.deepCopyJsonObject(this.panelConfigArea);
        delete this.panelArea.area_zones;
        delete this.panelArea.area_users;
        delete this.panelArea.arm_indication_to_output;
        delete this.panelArea.stay_arm_indication_to_output;
        delete this.panelArea.disarm_indication_to_output;
        delete this.panelArea.armed_exit_delay_beeps_to_output;
        delete this.panelArea.stay_exit_delay_beeps_to_output;
        delete this.panelArea.stay_arm_beeps_to_output;
        delete this.panelArea.arm_beeps_to_output;
        delete this.panelArea.pendant_tag_arm_beep_to_output;
        delete this.panelArea.pendant_tag_disarm_beep_to_output;
        delete this.panelArea.pendant_tag_stay_arm_beep_to_output;
        delete this.panelArea.pendant_tag_stay_disarm_beep_to_output;
        delete this.panelArea.disarm_beeps_to_output;
        delete this.panelArea.stay_disarm_beeps_to_output;
        delete this.panelArea.pulse_output_every_5_sec_when_disarmed;
        this.areaZones = {};
        this.areaUsers = {};
        this.armIndicationToOutput = {};
        this.stayArmIndicationToOutput = {};
        this.disarmIndicationToOutput = {};
        this.armBeepsToOutput = {};
        this.armedExitDelayBeepsToOutput = {};
        this.stayExitDelayBeepsToOutput = {};
        this.stayArmBeepsToOutput = {};
        this.pendantTagArmBeepToOutput = {};
        this.pendantTagDisarmBeepToOutput = {};
        this.pendantTagStayArmBeepToOutput = {};
        this.pendantTagStayDisarmBeepToOutput = {};
        this.disarmBeepsToOutput = {};
        this.stayDisarmBeepsToOutput = {};
        this.pulseOutputEvery5SecWhenDisarmed = {};

        for (let k in this.panelConfigArea.area_zones) {
            this.areaZones[k] = this.panelConfigArea.area_zones[k]["assigned"];
        }
        for (let k in this.panelConfigArea.area_users) {
            this.areaUsers[k] = this.panelConfigArea.area_users[k]["assigned"];
        }
        for (let k in this.panelConfigArea.arm_indication_to_output) {
            this.armIndicationToOutput[k] = this.panelConfigArea.arm_indication_to_output[k]["assigned"];
            this.stayArmIndicationToOutput[k] = this.panelConfigArea.stay_arm_indication_to_output[k]["assigned"];
            this.disarmIndicationToOutput[k] = this.panelConfigArea.disarm_indication_to_output[k]["assigned"];
            this.armedExitDelayBeepsToOutput[k] = this.panelConfigArea.armed_exit_delay_beeps_to_output[k]["assigned"];
            this.stayExitDelayBeepsToOutput[k] = this.panelConfigArea.stay_exit_delay_beeps_to_output[k]["assigned"];
            this.stayArmBeepsToOutput[k] = this.panelConfigArea.stay_arm_beeps_to_output[k]["assigned"];
            this.pendantTagArmBeepToOutput[k] = this.panelConfigArea.pendant_tag_arm_beep_to_output[k]["assigned"];
            this.pendantTagDisarmBeepToOutput[k] = this.panelConfigArea.pendant_tag_disarm_beep_to_output[k]["assigned"];
            this.pendantTagStayArmBeepToOutput[k] = this.panelConfigArea.pendant_tag_stay_arm_beep_to_output[k]["assigned"];
            this.pendantTagStayDisarmBeepToOutput[k] = this.panelConfigArea.pendant_tag_stay_disarm_beep_to_output[k]["assigned"];
            this.armBeepsToOutput[k] = this.panelConfigArea.arm_beeps_to_output[k]["assigned"];
            this.disarmBeepsToOutput[k] = this.panelConfigArea.disarm_beeps_to_output[k]["assigned"];
            this.stayDisarmBeepsToOutput[k] = this.panelConfigArea.stay_disarm_beeps_to_output[k]["assigned"];
            this.pulseOutputEvery5SecWhenDisarmed[k] = this.panelConfigArea.pulse_output_every_5_sec_when_disarmed[k]["assigned"];
        }
    }

    private _composeConfigArea() {
        let zones = Helper.deepCopyJsonObject(this.panelConfigArea.area_zones);
        let users = Helper.deepCopyJsonObject(this.panelConfigArea.area_users);
        let armIndicationToOutput = Helper.deepCopyJsonObject(this.panelConfigArea.arm_indication_to_output);
        let stayArmIndicationToOutput = Helper.deepCopyJsonObject(this.panelConfigArea.stay_arm_indication_to_output);
        let disarmIndicationToOutput = Helper.deepCopyJsonObject(this.panelConfigArea.disarm_indication_to_output);
        let armBeepsToOutput = Helper.deepCopyJsonObject(this.panelConfigArea.arm_beeps_to_output);
        let armedExitDelayBeepsToOutput = Helper.deepCopyJsonObject(this.panelConfigArea.armed_exit_delay_beeps_to_output);
        let stayExitDelayBeepsToOutput = Helper.deepCopyJsonObject(this.panelConfigArea.stay_exit_delay_beeps_to_output);
        let stayArmBeepsToOutput = Helper.deepCopyJsonObject(this.panelConfigArea.stay_arm_beeps_to_output);
        let pendantTagArmBeepToOutput = Helper.deepCopyJsonObject(this.panelConfigArea.pendant_tag_arm_beep_to_output);
        let pendantTagDisarmBeepToOutput = Helper.deepCopyJsonObject(this.panelConfigArea.pendant_tag_disarm_beep_to_output);
        let pendantTagStayArmBeepToOutput = Helper.deepCopyJsonObject(this.panelConfigArea.pendant_tag_stay_arm_beep_to_output);
        let pendantTagStayDisarmBeepToOutput = Helper.deepCopyJsonObject(this.panelConfigArea.pendant_tag_stay_disarm_beep_to_output);
        let disarmBeepsToOutput = Helper.deepCopyJsonObject(this.panelConfigArea.disarm_beeps_to_output);
        let stayDisarmBeepsToOutput = Helper.deepCopyJsonObject(this.panelConfigArea.stay_disarm_beeps_to_output);
        let pulseOutputEvery5SecWhenDisarmed = Helper.deepCopyJsonObject(this.panelConfigArea.pulse_output_every_5_sec_when_disarmed);

        this.panelConfigArea = Helper.deepCopyJsonObject(this.panelArea);

        for (let k in this.areaZones) {
            zones[k]["assigned"] = this.areaZones[k];
        }
        for (let k in this.areaUsers) {
            users[k]["assigned"] = this.areaUsers[k];
        }
        for (let k in this.armIndicationToOutput) {
            armIndicationToOutput[k]["assigned"] = this.armIndicationToOutput[k];
            stayArmIndicationToOutput[k]["assigned"] = this.stayArmIndicationToOutput[k];
            disarmIndicationToOutput[k]["assigned"] = this.disarmIndicationToOutput[k];
            armBeepsToOutput[k]["assigned"] = this.armBeepsToOutput[k];
            armedExitDelayBeepsToOutput[k]["assigned"] = this.armedExitDelayBeepsToOutput[k];
            stayExitDelayBeepsToOutput[k]["assigned"] = this.stayExitDelayBeepsToOutput[k];
            stayArmBeepsToOutput[k]["assigned"] = this.stayArmBeepsToOutput[k];
            pendantTagArmBeepToOutput[k]["assigned"] = this.pendantTagArmBeepToOutput[k];
            pendantTagDisarmBeepToOutput[k]["assigned"] = this.pendantTagDisarmBeepToOutput[k];
            pendantTagStayArmBeepToOutput[k]["assigned"] = this.pendantTagStayArmBeepToOutput[k];
            pendantTagStayDisarmBeepToOutput[k]["assigned"] = this.pendantTagStayDisarmBeepToOutput[k];
            disarmBeepsToOutput[k]["assigned"] = this.disarmBeepsToOutput[k];
            stayDisarmBeepsToOutput[k]["assigned"] = this.stayDisarmBeepsToOutput[k];
            pulseOutputEvery5SecWhenDisarmed[k]["assigned"] = this.pulseOutputEvery5SecWhenDisarmed[k];
        }

        this.panelConfigArea.area_zones = Helper.deepCopyJsonObject(zones);
        this.panelConfigArea.area_users = Helper.deepCopyJsonObject(users);
        this.panelConfigArea.arm_indication_to_output = Helper.deepCopyJsonObject(armIndicationToOutput);
        this.panelConfigArea.stay_arm_indication_to_output = Helper.deepCopyJsonObject(stayArmIndicationToOutput);
        this.panelConfigArea.disarm_indication_to_output = Helper.deepCopyJsonObject(disarmIndicationToOutput);
        this.panelConfigArea.arm_beeps_to_output = Helper.deepCopyJsonObject(armBeepsToOutput);
        this.panelConfigArea.armed_exit_delay_beeps_to_output = Helper.deepCopyJsonObject(armedExitDelayBeepsToOutput);
        this.panelConfigArea.stay_exit_delay_beeps_to_output = Helper.deepCopyJsonObject(stayExitDelayBeepsToOutput);
        this.panelConfigArea.stay_arm_beeps_to_output = Helper.deepCopyJsonObject(stayArmBeepsToOutput);
        this.panelConfigArea.pendant_tag_arm_beep_to_output = Helper.deepCopyJsonObject(pendantTagArmBeepToOutput);
        this.panelConfigArea.pendant_tag_disarm_beep_to_output = Helper.deepCopyJsonObject(pendantTagDisarmBeepToOutput);
        this.panelConfigArea.pendant_tag_stay_arm_beep_to_output = Helper.deepCopyJsonObject(pendantTagStayArmBeepToOutput);
        this.panelConfigArea.pendant_tag_stay_disarm_beep_to_output = Helper.deepCopyJsonObject(pendantTagStayDisarmBeepToOutput);
        this.panelConfigArea.disarm_beeps_to_output = Helper.deepCopyJsonObject(disarmBeepsToOutput);
        this.panelConfigArea.stay_disarm_beeps_to_output = Helper.deepCopyJsonObject(stayDisarmBeepsToOutput);
        this.panelConfigArea.pulse_output_every_5_sec_when_disarmed = Helper.deepCopyJsonObject(pulseOutputEvery5SecWhenDisarmed);

    }

    public save() {
        this.routerService.isLoading = true;
        this._composeConfigArea();
        this.panelAreaService.saveArea(this.panelId, this.panelConfigArea)
            .then(res => {
                this.routerService.simpleBackward('/panel-detail/' + this.panelId + '/areas');
            })
            .catch(error => {
                this.panelConfigArea = this.panelConfigAreaBackup;
                this.panelAreaService.restoreArea(this.panelId, this.panelConfigAreaBackup);
                alert(error);
                this.routerService.isLoading = false;
            });
    }

    public onNavBackBtnTap() {
        this.routerService.isLoading = true;
        this.routerService.simpleBackward('/panel-detail/' + this.panelId + '/areas');
    }
}