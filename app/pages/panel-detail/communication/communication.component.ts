import { Component, OnInit } from "@angular/core";
import { PageRoute } from "nativescript-angular/router";
import { CommunicationService } from "./communication.service";
import { Helper, PickType, PickValueConverter } from "./../../../shared/helper";
import { Config } from "./../../../shared/config";
import { Progress } from "ui/progress";
import { RouterService } from "../../../shared/router.service";

@Component({
    moduleId: module.id,
    selector: "communication",
    templateUrl: "./communication.html",
    styleUrls: ["./communication-common.css", "./communication.css"],
    providers: []
})

export class CommunicationComponent implements OnInit {
    public progressValue: number;
    public pairingDect: boolean;
    public pairingButtonText: string;
    public pairingInfo: string;

    public title: string;
    public configCommunication: any;
    public configCommunicationBackup: any;
    public panelId: number;
    public communicationDataForm: any;

    public securityTypes: Array<PickType>;
    public securityTypeConverter: PickValueConverter;
    public securityTypeNames: Array<String>;

    public isLoading: boolean;
    public labelTextSize: number;

    public keys(object: any) {
        return Helper.keys(object);
    }

    constructor(private pageRoute: PageRoute, private communicationService: CommunicationService, public routerService: RouterService) {
        this.routerService.isLoading = true;
        this.pageRoute.activatedRoute
            .switchMap(activatedRoute => activatedRoute.params)
            .forEach((params) => {
                this.panelId = +params["panelId"];
                this.title = "Panel " + this.panelId + ' > Communication';
            });
    }

    ngOnInit() {
        this.pairingButtonText = "Learn DECT Device";
        this.securityTypes = new Array<PickType>();
        for(let key in Config.securityTypes) {
            this.securityTypes.push(new PickType(+key, Config.securityTypes[+key]));
        }
        this.securityTypeConverter = new PickValueConverter(this.securityTypes);
        this.securityTypeNames = PickType.pickTypeNames(this.securityTypes);

        this.configCommunication = this.communicationService.getCommunication(this.panelId);
        this.configCommunicationBackup = this.configCommunication;
        this._deComposeConfigCommunication();
        this.labelTextSize = 16;
    }

    private _deComposeConfigCommunication() {
        this.communicationDataForm = Helper.deepCopyJsonObject(this.configCommunication);
        for(let k in this.configCommunication.dect_contact_number) {
            this.communicationDataForm['dect_contact_number' + '_'  + String(k)] = this.communicationDataForm.dect_contact_number[k];
        }
        for(let k in this.configCommunication.control_phone_numbers) {
            this.communicationDataForm['control_phone_numbers_'  + String(k)] = this.communicationDataForm.control_phone_numbers[k];
        }
        for(let k in this.configCommunication.wifi_ssid) {
            this.communicationDataForm[Helper.concatWithUnderscore('wifi_ssid', k)] = this.communicationDataForm.wifi_ssid[k];
            this.communicationDataForm[Helper.concatWithUnderscore('wifi_security_type', k)] = this.communicationDataForm.wifi_security_type[k];
            this.communicationDataForm[Helper.concatWithUnderscore('wifi_ssid_password', k)] = this.communicationDataForm.wifi_ssid_password[k];
        }
        delete this.communicationDataForm.control_phone_numbers;
        delete this.communicationDataForm.dect_contact_number;
        delete this.communicationDataForm.wifi_ssid;
        delete this.communicationDataForm.wifi_security_type;
        delete this.communicationDataForm.wifi_ssid_password;
    }

    private _composeConfigZone() {
        this.communicationDataForm['control_phone_numbers'] = {};
        this.communicationDataForm['dect_contact_number'] = {};
        this.communicationDataForm['wifi_ssid'] = new Array();
        this.communicationDataForm['wifi_security_type'] = new Array();
        this.communicationDataForm['wifi_ssid_password'] = new Array();
        for(let k in this.configCommunication.control_phone_numbers) {
            this.communicationDataForm['control_phone_numbers'][k] = this.communicationDataForm[Helper.concatWithUnderscore('control_phone_numbers', k)];
            delete this.communicationDataForm[Helper.concatWithUnderscore('control_phone_numbers', k)];
        }
        for(let k in this.configCommunication.dect_contact_number) {
            this.communicationDataForm['dect_contact_number'][k] = this.communicationDataForm[Helper.concatWithUnderscore('dect_contact_number', k)];
            delete this.communicationDataForm[Helper.concatWithUnderscore('dect_contact_number', k)];
        }
        for(let k in this.configCommunication.wifi_ssid) {
            this.communicationDataForm['wifi_ssid'][k] = this.communicationDataForm[Helper.concatWithUnderscore('wifi_ssid', k)];
            delete this.communicationDataForm[Helper.concatWithUnderscore('wifi_ssid', k)];
            this.communicationDataForm['wifi_security_type'][k] = this.communicationDataForm[Helper.concatWithUnderscore('wifi_security_type', k)];
            delete this.communicationDataForm[Helper.concatWithUnderscore('wifi_security_type', k)];
            this.communicationDataForm['wifi_ssid_password'][k] = this.communicationDataForm[Helper.concatWithUnderscore('wifi_ssid_password', k)];
            delete this.communicationDataForm[Helper.concatWithUnderscore('wifi_ssid_password', k)];
        }
        this.configCommunication = Helper.deepCopyJsonObject(this.communicationDataForm);
    }

    public save() {
        this.routerService.isLoading = true;
        this._composeConfigZone();
        this.communicationService.saveCommunication(this.panelId, this.configCommunication)
            .then(res => {
                this.routerService.simpleBackward('/panel-detail/' + this.panelId);
            })
            .catch(error => {
                this.configCommunication = this.configCommunicationBackup;
                this.communicationService.restoreCommunication(this.panelId, this.configCommunicationBackup);
                alert(error);
                this.routerService.isLoading = false;
            });
    }

    public onNavBackBtnTap() {
        this.routerService.isLoading = true;
        this.routerService.simpleBackward('/panel-detail/' + this.panelId);
    }
    public incrementString(str: string) {
        return String(+str + 1);
    }

    onValueChanged(args) {
        let progressBar = <Progress>args.object;
    }
    public learnDectDevice() {
        this.pairingDect = true;
        this.progressValue = 0;
        this.pairingButtonText = "Pairing Dect In Progress ...";
        this.pairingInfo = 'Level 1 in progress ...';
        return this.communicationService.learnDectDevice();
    }
    public deleteDectHanDevice() {
        return this.communicationService.deleteDectHanDevice();
    }
    public deleteDectHsDevice() {
        return this.communicationService.deleteDectHsDevice();
    }


    /*
    * https://api.crowcloud.xyz/panels/40/config_mode/
    *{"config":1}
    *
    *
    * https://api.crowcloud.xyz/panels/40/dect/ Request Method:POST
    * {"status":"learning"}
    * https://api.crowcloud.xyz/panels/40/dect/learn_state/1/ Request Method:GET
    * {"state":1}
    * https://api.crowcloud.xyz/panels/40/dect/learn_state/2/ Request Method:GET
    * {state: 2}
    * https://api.crowcloud.xyz/panels/40/dect/learn_state/3/ Request Method:GET
    * {state: 3}
    * https://api.crowcloud.xyz/panels/40/dect/ Request Method:DELETE
    * {status: "canceled"}
    *
    *
    * https://api.crowcloud.xyz/panels/40/dect/ Request Method:GET
    *
    * */
}