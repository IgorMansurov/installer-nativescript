import { Injectable } from "@angular/core";
import {PanelConfigService} from "../../../shared/panel/panel-config.service";

@Injectable()
export class CommunicationService {

    constructor(private panelConfigService: PanelConfigService) {}

    public saveCommunication(panelId: number, communication: any) {
        this.saveCommunicationLocally(panelId, communication);
        return this.panelConfigService.saveConfig();
    }
    public restoreCommunication(panelId: number, communication: any) {
        this.saveCommunicationLocally(panelId, communication);
    }
    public saveCommunicationLocally(panelId: number, communication: any) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        let keysToIgnore = [];

        for (let key in communication) {
            if(keysToIgnore.indexOf(key) === -1) {
                panelConfig[key] = communication[key];
            }
        }
        this.panelConfigService.saveConfigLocally(panelConfig);
    }

    public getCommunication(panelId: number) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        if(panelConfig) {
            return {
                remote_access_password: panelConfig.remote_access_password,
                remote_access_server_address: panelConfig.remote_access_server_address,
                remote_access_server_port: panelConfig.remote_access_server_port,
                server_registration_time: panelConfig.server_registration_time,
                Ethernet_Enabled: panelConfig.Ethernet_Enabled,
                DHCP_Enabled: panelConfig.DHCP_Enabled,
                dns_server: panelConfig.dns_server,
                tcp_remote_port: panelConfig.tcp_remote_port,
                static_ip: panelConfig.static_ip,
                subnet_mask: panelConfig.subnet_mask,
                gateway: panelConfig.gateway,
                GSM_IP_Enabled: panelConfig.GSM_IP_Enabled,
                GSM_CID_Enabled: panelConfig.GSM_CID_Enabled,
                gsm_apn: panelConfig.gsm_apn,
                ussd: panelConfig.ussd,
                pin_code: panelConfig.pin_code,
                gsm_user: panelConfig.gsm_user,
                gsm_password: panelConfig.gsm_password,
                control_phone_numbers: panelConfig.control_phone_numbers,
                WiFi_Enabled: panelConfig.WiFi_Enabled,
                wifi_ssid: panelConfig.wifi_ssid,
                wifi_security_type: panelConfig.wifi_security_type,
                wifi_ssid_password: panelConfig.wifi_ssid_password,
                DECT_Enabled: panelConfig.DECT_Enabled,
                dect_contact_number: panelConfig.dect_contact_number,
                dect_pin_code: panelConfig.dect_pin_code,
            }
        }
        else {
            return {};
        }
    }

    public learnDectDevice() {

    }
    public deleteDectHanDevice() {

    }
    public deleteDectHsDevice() {

    }
}