import {Injectable} from "@angular/core";
import {PanelConfigService} from "../../../shared/panel/panel-config.service";
import {Helper} from "./../../../shared/helper";

@Injectable()
export class PanelUserService {

    constructor(private panelConfigService: PanelConfigService) {
    }

    public saveUser(panelId: number, user: any) {
        this.saveUserLocally(panelId, user);
        return this.panelConfigService.saveConfig();
    }
    public restoreUser(panelId: number, user: any) {
        this.saveUserLocally(panelId, user);
    }
    public getUserNameById(panelId: number, index: number) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        return panelConfig.user_name[index];
    }
    public saveUserLocally(panelId: number, user: any) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        let keysToIgnore = ['id', 'user_assigned_to_area', 'user_code_turns_output_on', 'user_code_turns_output_off', 'pendant_panic_alarm_to_output', 'pendant_fire_alarm_to_output', 'pendant_medical_alarm_to_output', 'pendant_user_properties'];
        for (let key in user) {
            if(keysToIgnore.indexOf(key) === -1) {
                panelConfig[key][user.id] = user[key];
            }
        }
        for (let k in user.user_assigned_to_area) {
            panelConfig.user_assigned_to_area[k][user.id] = user.user_assigned_to_area[k]['assigned'];
        }
        for (let k in user.user_code_turns_output_on) {
            panelConfig.user_code_turns_output_on[k][user.id] = user.user_code_turns_output_on[k]['assigned'];
        }
        for (let k in user.user_code_turns_output_off) {
            panelConfig.user_code_turns_output_off[k][user.id] = user.user_code_turns_output_off[k]['assigned'];
        }
        for (let k in user.pendant_panic_alarm_to_output) {
            panelConfig.pendant_panic_alarm_to_output[k][user.id] = user.pendant_panic_alarm_to_output[k]['assigned'];
        }
        for (let k in user.pendant_fire_alarm_to_output) {
            panelConfig.pendant_fire_alarm_to_output[k][user.id] = user.pendant_fire_alarm_to_output[k]['assigned'];
        }
        for (let k in user.pendant_medical_alarm_to_output) {
            panelConfig.pendant_medical_alarm_to_output[k][user.id] = user.pendant_medical_alarm_to_output[k]['assigned'];
        }
        panelConfig.radio_pendants_list[user.id] = user.pendant_user_properties.radio_pendants_list;
        panelConfig.pendant_can_disarm_during_alarm_only[user.id] = user.pendant_user_properties.pendant_can_disarm_during_alarm_only;
        panelConfig.pendant_can_disarm_during_entry_delay_only_[user.id] = user.pendant_user_properties.pendant_can_disarm_during_entry_delay_only_;
        this.panelConfigService.saveConfigLocally(panelConfig);
    }
    public getUser(panelId: number, index: number) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        if(panelConfig) {
            let user_assigned_to_area = {};
            let user_code_turns_output_on = {};
            let user_code_turns_output_off = {};
            let pendant_panic_alarm_to_output = {};
            let pendant_fire_alarm_to_output = {};
            let pendant_medical_alarm_to_output = {};
            let pendantUser = {
                radio_pendants_list: panelConfig.radio_pendants_list[index],
                pendant_can_disarm_during_alarm_only: panelConfig.pendant_can_disarm_during_alarm_only[index],
                pendant_can_disarm_during_entry_delay_only_: panelConfig.pendant_can_disarm_during_entry_delay_only_[index],
            };
            for (let areaId in panelConfig.user_assigned_to_area) {
                user_assigned_to_area[areaId] = {name: panelConfig.area_name[areaId], assigned: panelConfig.user_assigned_to_area[areaId][index]};
            }
            for (let outputId in panelConfig.radio_outputs_list) {
                if (!Helper.isEmpty(panelConfig.radio_outputs_list[outputId])) {
                    user_code_turns_output_on[outputId] = {name: panelConfig.output_name[outputId], assigned: panelConfig.user_code_turns_output_on[outputId][index]};
                    user_code_turns_output_off[outputId] = {name: panelConfig.output_name[outputId], assigned: panelConfig.user_code_turns_output_off[outputId][index]};
                    pendant_panic_alarm_to_output[outputId] = {name: panelConfig.output_name[outputId], assigned: panelConfig.pendant_panic_alarm_to_output[outputId][index]};
                    pendant_fire_alarm_to_output[outputId] = {name: panelConfig.output_name[outputId], assigned: panelConfig.pendant_fire_alarm_to_output[outputId][index]};
                    pendant_medical_alarm_to_output[outputId] = {name: panelConfig.output_name[outputId], assigned: panelConfig.pendant_medical_alarm_to_output[outputId][index]};
                }
            }
            return {
                id: index,
                user_name: panelConfig.user_name[index],
                user_code: panelConfig.user_code[index],
                user_code_can_arm: panelConfig.user_code_can_arm[index],
                user_code_can_arm_stay: panelConfig.user_code_can_arm_stay[index],
                user_code_can_disarm: panelConfig.user_code_can_disarm[index],
                user_code_can_disarm_stay: panelConfig.user_code_can_disarm_stay[index],
                security_guard_user: panelConfig.security_guard_user[index],
                latchkey_mode_user: panelConfig.latchkey_mode_user[index],
                pendant_user: panelConfig.pendant_user[index],
                remote_control_user: panelConfig.remote_control_user[index],
                user_can_view_memory: panelConfig.user_can_view_memory[index],
                user_can_change_his_code: panelConfig.user_can_change_his_code[index],
                user_can_change_all_codes: panelConfig.user_can_change_all_codes[index],
                user_can_change_phone_or_address: panelConfig.user_can_change_phone_or_address[index],
                user_can_change_the_clock: panelConfig.user_can_change_the_clock[index],
                user_can_learn_radio_devices: panelConfig.user_can_learn_radio_devices[index],
                user_assigned_to_area: user_assigned_to_area,
                user_code_turns_output_on: user_code_turns_output_on,
                user_code_turns_output_off: user_code_turns_output_off,
                pendant_user_properties: pendantUser,
                pendant_panic_alarm_to_output: pendant_panic_alarm_to_output,
                pendant_fire_alarm_to_output: pendant_fire_alarm_to_output,
                pendant_medical_alarm_to_output: pendant_medical_alarm_to_output
            }
        }
        else {
            return {};
        }

    }
}