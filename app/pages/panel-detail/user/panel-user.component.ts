import { Component, OnInit, ViewChild } from "@angular/core";
import { PageRoute } from "nativescript-angular/router";
import { RouterService } from "../../../shared/router.service";
import { PanelUserService } from "./panel-user.service";
import { Helper, PendantUser } from "./../../../shared/helper";
import { BarcodeScanService } from "../../../shared/barcode-scan.service";

import { ButtonEditorHelper } from "../../../shared/button-helper";
import { RadDataFormComponent } from "nativescript-pro-ui/dataform/angular";
import { android as androidApplication } from "tns-core-modules/application";

import { TranslateService } from 'ng2-translate';

@Component({
    moduleId: module.id,
    selector: "panel-user",
    templateUrl: "./panel-user.html",
    styleUrls: ["./panel-user-common.css", "./panel-user.css"],
    providers: []
})

export class PanelUserComponent implements OnInit {

    public title: string;
    private _panelConfigUser: any;
    public panelConfigUserBackup: any;
    public panelUser: any;
    public user_assigned_to_area: any;
    public user_code_turns_output_on: any;
    public user_code_turns_output_off: any;
    public pendantUserProperties: any;
    public pendant_panic_alarm_to_output: any;
    public pendant_fire_alarm_to_output: any;
    public pendant_medical_alarm_to_output: any;
    public panelId: number;
    public panelUserId: number;
    public labelTextSize: number;
    public buttonBarcode: any;
    private _buttonEditorHelper;

    get panelConfigUser() {
        return this._panelConfigUser;
    }

    set panelConfigUser(v: any) {
        this._panelConfigUser = v;
    }

    public keys(object: any) {
        return Helper.keys(object);
    }

    constructor(private pageRoute: PageRoute, private panelUserService: PanelUserService, public routerService: RouterService,
                private barcodeScanService: BarcodeScanService, private translate: TranslateService) {
        this.pageRoute.activatedRoute
            .switchMap(activatedRoute => activatedRoute.params)
            .forEach((params) => {
                this.panelId = +params["panelId"];
                this.panelUserId = +params["userId"];
                this.translate.get('COMMON.PANEL').subscribe((res: string) => {
                    this.title = res + ' ' + this.panelId + ' > Users > ' + this.panelUserService.getUserNameById(this.panelId, this.panelUserId);
                });
            });
    }

    ngOnInit() {
        this.panelConfigUser = this.panelUserService.getUser(this.panelId, this.panelUserId);
        if (this.panelConfigUser) {
            this.panelConfigUserBackup = this.panelConfigUser;
            this._deComposeConfigUser();
        }
        else {
            this.panelUser = {};
        }
        this.labelTextSize = 16;
    }

    @ViewChild('pendantUserPropertiesForm') pendantUserPropertiesForm: RadDataFormComponent;

    private _deComposeConfigUser() {
        this.panelUser = Helper.deepCopyJsonObject(this.panelConfigUser);
        delete this.panelUser.user_assigned_to_area;
        delete this.panelUser.user_code_turns_output_on;
        delete this.panelUser.user_code_turns_output_off;
        delete this.panelUser.pendant_user_properties;
        delete this.panelUser.pendant_panic_alarm_to_output;
        delete this.panelUser.pendant_fire_alarm_to_output;
        delete this.panelUser.pendant_medical_alarm_to_output;
        this.user_assigned_to_area = {};
        this.user_code_turns_output_on = {};
        this.user_code_turns_output_off = {};
        this.pendant_panic_alarm_to_output = {};
        this.pendant_fire_alarm_to_output = {};
        this.pendant_medical_alarm_to_output = {};

        this.pendantUserProperties = new PendantUser();
        this.pendantUserProperties = this.panelConfigUser.pendant_user_properties;
        this.pendantUserProperties.buttonBarcode = 0;

        for (let k in this.panelConfigUser.user_assigned_to_area) {
            this.user_assigned_to_area[k] = this.panelConfigUser.user_assigned_to_area[k]["assigned"];
        }
        for (let k in this.panelConfigUser.user_code_turns_output_on) {
            this.user_code_turns_output_on[k] = this.panelConfigUser.user_code_turns_output_on[k]["assigned"];
        }
        for (let k in this.panelConfigUser.user_code_turns_output_off) {
            this.user_code_turns_output_off[k] = this.panelConfigUser.user_code_turns_output_off[k]["assigned"];
        }
        for (let k in this.panelConfigUser.pendant_panic_alarm_to_output) {
            this.pendant_panic_alarm_to_output[k] = this.panelConfigUser.pendant_panic_alarm_to_output[k]["assigned"];
        }
        for (let k in this.panelConfigUser.pendant_fire_alarm_to_output) {
            this.pendant_fire_alarm_to_output[k] = this.panelConfigUser.pendant_fire_alarm_to_output[k]["assigned"];
        }
        for (let k in this.panelConfigUser.pendant_medical_alarm_to_output) {
            this.pendant_medical_alarm_to_output[k] = this.panelConfigUser.pendant_medical_alarm_to_output[k]["assigned"];
        }
    }

    private _composeConfigUser() {
        let user_assigned_to_area = Helper.deepCopyJsonObject(this.panelConfigUser.user_assigned_to_area);
        let outputsOn = Helper.deepCopyJsonObject(this.panelConfigUser.user_code_turns_output_on);
        let outputsOff = Helper.deepCopyJsonObject(this.panelConfigUser.user_code_turns_output_off);
        let pendantAlarmOutputs = Helper.deepCopyJsonObject(this.panelConfigUser.pendant_panic_alarm_to_output);
        let pendantFireOutputs = Helper.deepCopyJsonObject(this.panelConfigUser.pendant_fire_alarm_to_output);
        let pendantMedicalOutputs = Helper.deepCopyJsonObject(this.panelConfigUser.pendant_medical_alarm_to_output);

        this.panelConfigUser = Helper.deepCopyJsonObject(this.panelUser);
        for (let k in this.user_assigned_to_area) {
            user_assigned_to_area[k]["assigned"] = this.user_assigned_to_area[k];
        }
        for (let k in this.user_code_turns_output_on) {
            outputsOn[k]["assigned"] = this.user_code_turns_output_on[k];
        }
        for (let k in this.user_code_turns_output_off) {
            outputsOff[k]["assigned"] = this.user_code_turns_output_off[k];
        }
        for (let k in this.pendant_panic_alarm_to_output) {
            pendantAlarmOutputs[k]["assigned"] = this.pendant_panic_alarm_to_output[k];
        }
        for (let k in this.pendant_fire_alarm_to_output) {
            pendantFireOutputs[k]["assigned"] = this.pendant_fire_alarm_to_output[k];
        }
        for (let k in this.pendant_medical_alarm_to_output) {
            pendantMedicalOutputs[k]["assigned"] = this.pendant_medical_alarm_to_output[k];
        }
        this.panelConfigUser.user_assigned_to_area = Helper.deepCopyJsonObject(user_assigned_to_area);
        this.panelConfigUser.user_code_turns_output_on = Helper.deepCopyJsonObject(outputsOn);
        this.panelConfigUser.user_code_turns_output_off = Helper.deepCopyJsonObject(outputsOff);
        delete this.pendantUserProperties.buttonBarcode;
        this.panelConfigUser.pendant_user_properties = Helper.deepCopyJsonObject(this.pendantUserProperties);
        this.panelConfigUser.pendant_panic_alarm_to_output = Helper.deepCopyJsonObject(pendantAlarmOutputs);
        this.panelConfigUser.pendant_fire_alarm_to_output = Helper.deepCopyJsonObject(pendantFireOutputs);
        this.panelConfigUser.pendant_medical_alarm_to_output = Helper.deepCopyJsonObject(pendantMedicalOutputs);
    }

    public save() {
        this.routerService.isLoading = true;
        this.pendantUserPropertiesForm.dataForm.commitAll();
        this._composeConfigUser();
        this.panelUserService.saveUser(this.panelId, this.panelConfigUser)
            .then(res => {
                this.routerService.simpleBackward('/panel-detail/' + this.panelId + '/users');
            })
            .catch(error => {
                this.panelConfigUser = this.panelConfigUserBackup;
                this.panelUserService.restoreUser(this.panelId, this.panelConfigUserBackup);
                alert(error);
                this.routerService.isLoading = false;
            });
    }

    public onNavBackBtnTap() {
        this.routerService.isLoading = true;
        this.routerService.simpleBackward('/panel-detail/' + this.panelId + '/users');
    }

    public editorNeedsView(args) {
        if (androidApplication) {
            this._buttonEditorHelper = new ButtonEditorHelper();
            this._buttonEditorHelper.editor = args.object;
            let androidEditorView: android.widget.Button = new android.widget.Button(args.context);
            let that = this;
            androidEditorView.setOnClickListener(new android.view.View.OnClickListener({
                onClick(view: android.view.View) {
                    that.handleTap(view, args.object);
                }
            }));
            args.view = androidEditorView;
            this.updateEditorValue(androidEditorView, this.pendantUserProperties.radio_pendants_list);
        } else {
            this._buttonEditorHelper = new ButtonEditorHelper();
            this._buttonEditorHelper.editor = args.object;
            let iosEditorView = UIButton.buttonWithType(UIButtonType.System);
            iosEditorView.contentHorizontalAlignment = UIControlContentHorizontalAlignment.Left;
            iosEditorView.addTargetActionForControlEvents(this._buttonEditorHelper, "handleTap:", UIControlEvents.TouchUpInside);
            args.view = iosEditorView;
        }
    }

    public editorHasToApplyValue(args) {
        this._buttonEditorHelper.updateEditorValue(args.view, args.value);
    }

    public editorNeedsValue(args) {
        args.value = this._buttonEditorHelper.buttonValue;
    }

    public updateEditorValue(editorView, value) {
        this._buttonEditorHelper.buttonValue = value;
        editorView.setText("Tap to scan pendant barcode");
    }

    public handleTap(editorView, editor) {
        this.barcodeScanService.scanBarcode()
            .then((result) => {
                    this.updateEditorValue(editorView, result.text);
                    editor.notifyValueChanged();
                    /* TODO
                    https://github.com/telerik/nativescript-ui-feedback/issues/344
                    * */
                    Helper.reloadRadDataFormProperties(this, 'pendantUserPropertiesForm', 'pendantUserProperties', {
                        "radio_pendants_list": +result.text
                    });
                }, (errorMessage) => {
                    alert("No scan. " + errorMessage);
                    console.log("No scan. " + errorMessage);
                }
            )
            .catch((error) => alert('Error scan barcode.. ' + error));
    }

}