import { Component, OnInit } from "@angular/core";
import { PageRoute } from "nativescript-angular/router";
import { RouterService } from "../../../shared/router.service";
import { PanelUsersService } from "../../../shared/panel/panel-users.service";

@Component({
  moduleId: module.id,
  selector: "panel-users",
  templateUrl: "./panel-users.html",
  styleUrls: ["./panel-users-common.css", "./panel-users.css"],
  providers: []
})
export class PanelUsersComponent implements OnInit {

  public users: any;
  public panelId: number;
  public title: string;

  /*
    TODO: https://github.com/bradmartin/nativescript-floatingactionbutton/issues/61
  */

  constructor(private panelUsersService: PanelUsersService, private pageRoute: PageRoute, public routerService: RouterService) {
    this.routerService.isLoading = true;
    this.pageRoute.activatedRoute
      .switchMap(activatedRoute => activatedRoute.params)
      .forEach((params) => {
        this.panelId = +params["panelId"];
        this.title = "Panel " + this.panelId + ' > Users';
    });
  }
  ngOnInit() {
    this.users = this.panelUsersService.getPanelUserList(this.panelId);
  }
  public showAddUserButton() {
    return this.panelUsersService.quantityEmptyUserCodes(this.panelId) > 0;
  }
  public editUser(userId: number = -1) {
    this.routerService.isLoading = true;
    if(userId === -1) {
      userId = this.panelUsersService.getFirstEmptyUser(this.panelId);
    }
    this.routerService.simpleForward('/panel-detail/' + this.panelId + '/users/' + userId);
  }

  public onNavBackBtnTap() {
      this.routerService.isLoading = true;
      this.routerService.simpleBackward('/panel-detail/' + this.panelId);
  }

  public increaseIndex(index: any) {
       return String(Number(index) + 1);
  }
}