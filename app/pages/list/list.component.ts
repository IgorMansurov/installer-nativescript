import { Component, OnInit } from "@angular/core";
import { Panel } from "../../shared/panel/panel";
import { PanelListService } from "../../shared/panel/panel-list.service";
import { PanelConfigService } from "../../shared/panel/panel-config.service";
import { RouterService } from "../../shared/router.service";

import { Config } from "../../shared/config";
import { Helper } from "../../shared/helper";

import { ViewContainerRef } from "@angular/core";
import { ModalDialogService } from "nativescript-angular/directives/dialogs";
import { InstallerCodeComponent } from "./../../pages/modals/installer-code/installer-code.component";
import { SearchBar } from "ui/search-bar";

import {TranslateService} from 'ng2-translate';

@Component({
    moduleId: module.id,
    selector: "list",
    templateUrl: "./list.html",
    styleUrls: ["./list-common.css", "./list.css"],
    providers: [PanelListService]
})
export class ListComponent implements OnInit {

    public panelList: Array<Panel>;
    public configLoaded: boolean;
    public pageNumber: number;
    public pageSize: number;
    public enriched: number;
    public nextPage: boolean;
    public search: string;

    constructor(private panelConfigService: PanelConfigService, private panelListService: PanelListService, public routerService: RouterService,
                private modal: ModalDialogService, private vcRef: ViewContainerRef, private translate: TranslateService) {

        this.routerService.isLoading = true;
    }

    ngOnInit() {
        this.panelList = [];
        this.configLoaded = false;
        this.pageNumber = Config.pageNumber;
        this.pageSize = Config.pageSize;
        this.enriched = 1;
        this.nextPage = true;
        this.search = '';
        this.loadMoreItems();
    }

    public getAlignment() {
        return this.translate.currentLang == 'he' ? 'right' : 'left';
    }

    public onSubmit(args) {
        let searchBar = <SearchBar>args.object;
        let searchValue = searchBar.text.toLowerCase();
        this.panelList = [];
        this.search = searchValue;
        this.pageNumber = 1;
        this.loadMoreItems();
    }

    public onClear(args) {
        let searchBar = <SearchBar>args.object;
        searchBar.text = "";
        searchBar.hint = "";

        this.translate.get('LIST.SEARCH_PANEL').subscribe((res: string) => {
            searchBar.hint = res;
        });

        this.panelList = [];
        this.search = '';
        this.pageNumber = 1;
        this.loadMoreItems();
    }

    public addPanel() {
        return this.routerService.simpleForward('/panel-add');
    }

    public loadMoreItems() {
        this.routerService.isLoading = true;
        this.panelListService.load({enriched: this.enriched, pageNumber: this.pageNumber, pageSize: this.pageSize, search: this.search})
            .subscribe(loadedPanels => {
                    if (loadedPanels.length < this.pageSize) {
                        this.nextPage = false;
                    }
                    else {
                        this.pageNumber += 1;
                        this.nextPage = true;
                    }
                    loadedPanels.forEach((panelObject) => {
                        this.panelList.push(panelObject);
                    });
                    this.routerService.isLoading = false;
                },
                () => {
                    this.routerService.isLoading = false;
                    alert("Unfortunately we were unable to load your panel list..");
                    return this.routerService.simpleForward('');
                }
            );
    }

    public getInstallerCode() {
        let options = {
            context: {},
            fullscreen: false,
            viewContainerRef: this.vcRef
        };
        return this.modal.showModal(InstallerCodeComponent, options).then(res => {
            this.routerService.isLoading = true;
            this.panelConfigService.setInstallerCode(res);
        });
    }

    public panelDetail(panelId: number) {
        this.getInstallerCode()
            .then(() => this.panelConfigService.initConfig(panelId))
            .then(() => {
                return this.routerService.simpleForward('/panel-detail/' + panelId);
            })
            .catch(error => {
                alert(error);
                return this.routerService.simple('/list');
            });
    }

    public onNavBackBtnTap() {
        return this.routerService.simpleBackward('');
    }

    public isVisibleField(obj: any, key: any) {
        return Helper.isVisibleField(obj, key);
    }
}