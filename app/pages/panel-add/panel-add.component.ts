import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import {BarcodeScanner} from "nativescript-barcodescanner";

import * as geolocation from "nativescript-geolocation";
import { Accuracy } from "ui/enums";

//import * as PushNotifications from "nativescript-push-notifications";


@Component({
    moduleId: module.id,
    selector: "panel-add",
    templateUrl: "./panel-add.html",
    styleUrls: ["./panel-add-common.css", "./panel-add.css"]
})
export class PanelAddComponent implements OnInit {

  panelMac: string;
  panelName: string;
  panelRemoteAccessPassword: string;
  panelUserCode: string;
  myLocation: any;
  watchId: any;
  pushToken: any;
  pushData: any;
  barcodeScanner: any;

  @ViewChild("macaddress") macAddress: ElementRef;

  constructor() {
      this.barcodeScanner = new BarcodeScanner();
      let settings = {
            senderID: "891196473559",
            badge: true,
            sound: true,
            alert: true,
            interactiveSettings: {
                actions: [{
                    identifier: 'READ_IDENTIFIER',
                    title: 'Read',
                    activationMode: "foreground",
                    destructive: false,
                    authenticationRequired: true
                }, {
                    identifier: 'CANCEL_IDENTIFIER',
                    title: 'Cancel',
                    activationMode: "foreground",
                    destructive: true,
                    authenticationRequired: true
                }],
                categories: [{
                    identifier: 'READ_CATEGORY',
                    actionsForDefaultContext: ['READ_IDENTIFIER', 'CANCEL_IDENTIFIER'],
                    actionsForMinimalContext: ['READ_IDENTIFIER', 'CANCEL_IDENTIFIER']
                }]
            },
            notificationCallbackIOS: data => {
                console.log("DATA: " + JSON.stringify(data));
            },
            notificationCallbackAndroid: (message, data, notification) => {

                console.log("MESSAGE: " + JSON.stringify(message));
                console.log("DATA: " + JSON.stringify(data));


                this.pushData = JSON.stringify(message);


                console.log("NOTIFICATION: " + JSON.stringify(notification));
            }
        };
      /*
        PushNotifications.register(settings, data => {
            console.log("REGISTRATION ID: " + JSON.stringify(data));
            this.pushToken = JSON.stringify(data);
            PushNotifications.onMessageReceived(settings.notificationCallbackAndroid);
        }, error => {
            console.log(error);
        });
        */
  }

  ngOnInit() {}

  public scanBarcode() {
    this.barcodeScanner.scan({
        formats: "QR_CODE, EAN_13",
        cancelLabel: "EXIT. Also, try the volume buttons!", // iOS only, default 'Close'
        cancelLabelBackgroundColor: "#333333", // iOS only, default '#000000' (black)
        message: "Use the volume buttons for extra light", // Android only, default is 'Place a barcode inside the viewfinder rectangle to scan it.'
        showFlipCameraButton: true,   // default false
        preferFrontCamera: false,     // default false
        showTorchButton: true,        // default false
        beepOnScan: true,             // Play or Suppress beep on scan (default true)
        torchOn: false,               // launch with the flashlight on (default false)
        resultDisplayDuration: 500,   // Android only, default 1500 (ms), set to 0 to disable echoing the scanned text
        orientation: 'orientation',     // Android only, default undefined (sensor-driven orientation), other options: portrait|landscape
        openSettingsIfPermissionWasPreviouslyDenied: true // On iOS you can send the user to the settings app if access was previously denied
      }).then((result) => {
          // Note that this Promise is never invoked when a 'continuousScanCallback' function is provided
            this.panelMac = result.text;
            }, (errorMessage) => {
                alert("No scan. " + errorMessage);
                console.log("No scan. " + errorMessage);
            }
        );
  }

  public save() {
  }

  public addPushNotifications() {
      /*
    var pushPlugin = require("nativescript-push-notifications");

	pushPlugin.register({ senderID: '891196473559' }, function (data){
		console.log("message", "" + JSON.stringify(data));
	}, function() { });

	pushPlugin.onMessageReceived(function callback(data) {
		console.log(data);
	});

	pushPlugin.areNotificationsEnabled((data) => {
	    console.log("message", "" + JSON.stringify(data));
    })
      */
  }


  public getCurrentLocation() {
      let that = this;
      let location = geolocation.getCurrentLocation({ desiredAccuracy: Accuracy.high })
        .then(function(loc) {
            if (loc) {
                that.myLocation = loc.latitude;
            }
        }, function(e) {
            console.log("Error: " + e.message);
            alert("Error: " + e.message);

    });
  }

  public watchLocation() {
      let that = this;
      this.watchId = geolocation.watchLocation(
          function (loc) {
              if (loc) {
                  that.myLocation = loc.longitude;
              }
          },
          function (e) {
              console.log("Error: " + e.message);
              alert("Error: " + e.message);
          },
          {desiredAccuracy: Accuracy.high, updateDistance: 0.1, minimumUpdateTime: 100});
  }

}