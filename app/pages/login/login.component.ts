import { Page } from "ui/page";
import { User } from "../../shared/user/user";
import { UserService } from "../../shared/user/user.service";
import { RouterService } from "../../shared/router.service";

import { FingerprintAuth } from "nativescript-fingerprint-auth";

import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { View } from "ui/core/view";

import { getString, setString } from "application-settings";

import * as Platform from "platform";
import { TranslateService } from 'ng2-translate';

import { ListPicker } from "ui/list-picker";
import { Config } from "./../../shared/config";

@Component({
    moduleId: module.id,
    selector: "my-app",
    providers: [UserService],
    templateUrl: "./login.html",
    styleUrls: ["./login-common.css", "./login.css"]
})
export class LoginComponent implements OnInit {

    private fingerprintAuth: FingerprintAuth;
    public fingerPrintAvailable: boolean;
    user: User;
    isEmailInLocalStorage: boolean;

    public langs: Array<any>;


    @ViewChild("logoCrow") logoCrow: ElementRef;

    constructor(private userService: UserService, private page: Page, public routerService: RouterService, private translate: TranslateService) {
        this.translate.setDefaultLang("en");
        this.translate.use(Platform.device.language);
        this.fingerprintAuth = new FingerprintAuth();
        this.user = new User();
        this.user.email = '';
        this.user.password = '';
        this.getCredentialsFromLocalStorage();
    }
    public changeLanguage(args: any) {
        let picker = <ListPicker>args.object;
        this.translate.use(this.langs[picker.selectedIndex]);
    }

    ngOnInit() {
        this.routerService.isLoading = false;
        this.langs = Config.langs;
        let logoCrow = <View>this.logoCrow.nativeElement;
        this.fingerprintAuth.available()
            .then(
                (avail: any) => {
                    //this.fingerPrintAvailable = avail;
                    this.fingerPrintAvailable = false;
                }
            );
        this.page.actionBarHidden = true;
    }

    public getCredentialsFromLocalStorage() {
        this.user.email = getString('email');
        this.user.password = getString('password');
    }

    public doVerifyFingerprintWithCustomFallback(): void {
        this.fingerprintAuth.verifyFingerprintWithCustomFallback({
            message: 'Scan your finger', // optional
            fallbackMessage: 'Enter PIN' // optional
        }).then(
            () => {
                this.getCredentialsFromLocalStorage();
                this.loginUser();
            },
            (error) => {
                alert({
                    title: "Fingerprint NOT OK",
                    message: (error.code === -3 ? "Show custom fallback" : error.message),
                    okButtonText: "NOT OK"
                });
            }
        );
    }

    public loginUser() {
        if (this.user.email === '' || this.user.password === '') {
            alert('Please, provide login and password..');
        }
        else {
            this.routerService.isLoading = true;
            let logoCrow = <View>this.logoCrow.nativeElement;
            //this.imagePulsation(logoCrow, 1.5);
            return this.userService.loginUser(this.user)
                .then((res) => {
                        setString('email', this.user.email);
                        setString('password', this.user.password);
                        return this.routerService.simpleForward('/list');
                    }
                )
                .catch((error) => {
                    alert(error);
                    this.routerService.isLoading = false;
                });
        }
    }

    public imagePulsation(image: any, scale: number) {
        if (this.routerService.isLoading) {
            if (scale === 1.5) {
                let animationIncrease = image.createAnimation({scale: {x: 1.5, y: 1.5}, duration: 2000});
                animationIncrease.play()
                    .then(() => this.imagePulsation(image, 1));
            }
            else {
                let animationDecrease = image.createAnimation({scale: {x: 1, y: 1}, duration: 2000});
                animationDecrease.play()
                    .then(() => this.imagePulsation(image, 1.5));
            }
        }
        else {
            let animationDecrease = image.createAnimation({scale: {x: 1, y: 1}, duration: 2000});
            animationDecrease.play();
        }
    }
}