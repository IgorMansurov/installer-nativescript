import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { NativeScriptHttpModule } from "nativescript-angular/http";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import {TranslateModule, TranslateLoader, TranslateStaticLoader} from "ng2-translate";
import {Http} from "@angular/http";
// for AoT compilation
export function translateLoaderFactory(http: Http) {
    return new TranslateStaticLoader(http, "/i18n", ".json");
};


import { AppComponent } from "./app.component";
//import { routes, navigatableComponents } from "./app.routing";

import { BarcodeScanner } from 'nativescript-barcodescanner';
import { BarcodeScanService } from "./shared/barcode-scan.service";
import { RouterService } from "./shared/router.service";

import { PanelConfigService } from "./shared/panel/panel-config.service";
import { PanelUsersService } from "./shared/panel/panel-users.service";
import { PanelUserService } from "./pages/panel-detail/user/panel-user.service";
import { PanelOutputsService } from "./shared/panel/panel-outputs.service";
import { PanelOutputService } from "./pages/panel-detail/output/panel-output.service";
import { PanelAreasService } from "./shared/panel/panel-areas.service";
import { PanelAreaService } from "./pages/panel-detail/area/panel-area.service";
import { PanelZonesService } from "./shared/panel/panel-zones.service";
import { PanelZoneService } from "./pages/panel-detail/zone/panel-zone.service";
import { ReportChannelsService } from "./pages/panel-detail/report-channels/report-channels.service";
import { ReportChannelService } from "./pages/panel-detail/report-channel/report-channel.service";
import { KeypadsService } from "./pages/panel-detail/keypads/keypads.service";
import { KeypadService } from "./pages/panel-detail/keypad/keypad.service";
import { CommunicationService } from "./pages/panel-detail/communication/communication.service";
import { MiscellaneousService } from "./pages/panel-detail/miscellaneous/miscellaneous.service";
import { DiagnosticService } from "./pages/panel-detail/diagnostic/diagnostic.service";
import { NativeScriptUIDataFormModule } from "nativescript-pro-ui/dataform/angular";

import { ModalDialogService } from "nativescript-angular/modal-dialog";
import { InstallerCodeComponent } from "./pages/modals/installer-code/installer-code.component";


import { LoginComponent } from "./pages/login/login.component";
import { ListComponent } from "./pages/list/list.component";
import { PanelDetailComponent } from "./pages/panel-detail/panel-detail.component";
import { PanelUsersComponent } from "./pages/panel-detail/users/panel-users.component";
import { PanelUserComponent } from "./pages/panel-detail/user/panel-user.component";
import { PanelAddComponent } from "./pages/panel-add/panel-add.component";
import { PanelAreasComponent } from "./pages/panel-detail/areas/panel-areas.component";
import { PanelAreaComponent } from "./pages/panel-detail/area/panel-area.component";
import { PanelZonesComponent } from "./pages/panel-detail/zones/panel-zones.component";
import { PanelZoneComponent } from "./pages/panel-detail/zone/panel-zone.component";
import { PanelOutputsComponent } from "./pages/panel-detail/outputs/panel-outputs.component";
import { PanelOutputComponent } from "./pages/panel-detail/output/panel-output.component";
import { ReportChannelsComponent } from "./pages/panel-detail/report-channels/report-channels.component";
import { ReportChannelComponent } from "./pages/panel-detail/report-channel/report-channel.component";
import { KeypadsComponent } from "./pages/panel-detail/keypads/keypads.component";
import { KeypadComponent } from "./pages/panel-detail/keypad/keypad.component";
import { CommunicationComponent } from "./pages/panel-detail/communication/communication.component";
import { MiscellaneousComponent } from "./pages/panel-detail/miscellaneous/miscellaneous.component";
import { DiagnosticComponent } from "./pages/panel-detail/diagnostic/diagnostic.component";


export const routes = [
  { path: "", component: LoginComponent },
  { path: "list", component: ListComponent },
  { path: "panel-detail/:panelId", component: PanelDetailComponent },
  { path: "panel-detail/:panelId/users", component: PanelUsersComponent },
  { path: "panel-detail/:panelId/areas", component: PanelAreasComponent },
  { path: "panel-detail/:panelId/zones", component: PanelZonesComponent },
  { path: "panel-detail/:panelId/outputs", component: PanelOutputsComponent },
  { path: "panel-detail/:panelId/report-channels", component: ReportChannelsComponent },
  { path: "panel-detail/:panelId/keypads", component: KeypadsComponent },
  { path: "panel-detail/:panelId/communication", component: CommunicationComponent },
  { path: "panel-detail/:panelId/miscellaneous", component: MiscellaneousComponent },
  { path: "panel-detail/:panelId/diagnostic", component: DiagnosticComponent },
  { path: "panel-detail/:panelId/users/:userId", component: PanelUserComponent },
  { path: "panel-detail/:panelId/areas/:areaId", component: PanelAreaComponent },
  { path: "panel-detail/:panelId/zones/:zoneId", component: PanelZoneComponent },
  { path: "panel-detail/:panelId/outputs/:outputId", component: PanelOutputComponent },
  { path: "panel-detail/:panelId/report-channels/:reportChannelId", component: ReportChannelComponent },
  { path: "panel-detail/:panelId/keypads/:keypadId", component: KeypadComponent },
  { path: "panel-add", component: PanelAddComponent },
];

export const navigatableComponents = [
  LoginComponent,
  ListComponent,
  PanelDetailComponent,
  PanelUsersComponent,
  PanelUserComponent,
  PanelAddComponent,
  PanelAreasComponent,
  PanelAreaComponent,
  PanelZonesComponent,
  PanelZoneComponent,
  PanelOutputsComponent,
  PanelOutputComponent,
  ReportChannelsComponent,
  ReportChannelComponent,
  KeypadsComponent,
  KeypadComponent,
  CommunicationComponent,
  MiscellaneousComponent,
  DiagnosticComponent
];


@NgModule({
  entryComponents: [InstallerCodeComponent],
  schemas: [NO_ERRORS_SCHEMA],
  imports: [
    NativeScriptUIDataFormModule,
    NativeScriptModule,
    NativeScriptFormsModule,
    NativeScriptHttpModule,
    NativeScriptRouterModule,
    NativeScriptRouterModule.forRoot(routes),
    TranslateModule.forRoot([{
    provide: TranslateLoader,
    deps: [Http],
    useFactory: (translateLoaderFactory)}])
  ],
  declarations: [
    AppComponent,
    InstallerCodeComponent,
    ...navigatableComponents
  ],
  providers: [
      ModalDialogService,
      BarcodeScanner,
      BarcodeScanService,
      RouterService,
      PanelConfigService,
      PanelUsersService,
      PanelUserService,
      PanelOutputsService,
      PanelOutputService,
      PanelAreasService,
      PanelAreaService,
      PanelZonesService,
      PanelZoneService,
      ReportChannelsService,
      ReportChannelService,
      KeypadsService,
      KeypadService,
      CommunicationService,
      MiscellaneousService,
      DiagnosticService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}