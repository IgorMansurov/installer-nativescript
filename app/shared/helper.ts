import {PropertyConverter} from "nativescript-pro-ui/dataform";

export class Helper {
    public static deepCopyJsonObject(object: any) {
        return JSON.parse(JSON.stringify(object));
    }

    public static keys(object: any): Array<string> {
        if (object) {
            return Object.keys(object);
        }
        else {
            return [];
        }
    }

    public static isEmpty(field: any) {
        return field === "" || field === 0 || field === '0';
    }

    public static reloadRadDataFormProperties(that: any, formId: string, sourceName: string, newValues: any = null) {
        let newObject = {};
        newObject = Helper.deepCopyJsonObject(that[sourceName]);
        if (newValues) {
            for (let k in newValues) {
                newObject[k] = newValues[k];
            }
        }
        that[sourceName] = newObject;
        that[formId].dataForm.reload();
    }

    public static concatWithUnderscore(left: any, right: any) {
        return String(left) + '_' + String(right);
    }

    public static isVisibleField(obj: any, key: any) {
        if (typeof obj[key] === "number") {
            return true;
        }
        else if (typeof obj[key] === "string") {
            return obj[key].length > 0;
        }
        return false;
    }
}

export class PendantUser {
    public radio_pendants_list: number;
    public pendant_can_disarm_during_alarm_only: boolean;
    public pendant_can_disarm_during_entry_delay_only_: boolean;
}

export class ZoneMain {
    public zone_name: string;
    public radio_zones_list: number;
}

export class WorkingMode {
    public id: number;
    public name: string;

    constructor(id: number, name: string) {
        this.id = id;
        this.name = name;
    }
}

export class WorkingModeConverter implements PropertyConverter {
    constructor(private _workingModes: Array<WorkingMode>) {
    }

    convertFrom(id: number) {
        return this._workingModes.filter((mode: WorkingMode) => mode.id == id)[0].name;
    }

    convertTo(name: string) {
        return this._workingModes.filter((mode: WorkingMode) => mode.name == name)[0].id;
    }
}

export class OutputMain {
    public output_name: number;
    public radio_outputs_list: number;
}

export class PickType {
    public id: number;
    public name: string;

    constructor(id: number, name: string) {
        this.id = id;
        this.name = name;
    }

    public static pickTypeNames(pickTypes: Array<PickType>) {
        return pickTypes.map((value: PickType) => value.name);
    }
}

export class PickValueConverter implements PropertyConverter {
    constructor(private _types: Array<PickType>) {
    }

    convertFrom(id: number) {
        return this._types.filter((mode: PickType) => mode.id == id)[0].name;
    }

    convertTo(name: string) {
        return this._types.filter((mode: PickType) => mode.name == name)[0].id;
    }
}
