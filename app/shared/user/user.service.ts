import {Injectable} from "@angular/core";
import {Http, Headers, Response, URLSearchParams} from "@angular/http";
import {Observable} from "rxjs/Rx";
import "rxjs/add/operator/do";
import "rxjs/add/operator/map";

import {User} from "./user";
import {Config} from "../config";

@Injectable()
export class UserService {
    constructor(private http: Http) {
    }

    /*
      register(user: User) {
        let headers = new Headers();
        headers.append("Content-Type", "application/json");

        return this.http.post(
          Config.apiUrl + "Users",
          JSON.stringify({
            Username: user.email,
            Email: user.email,
            Password: user.password
          }),
          { headers: headers }
        )
        .catch(this.handleErrors);
      }

      login(user: User) {
        let headers = new Headers();
        headers.append('Authorization', Config.authHeader);
        headers.append('Content-Type', 'application/x-www-form-urlencoded');

        let params = new URLSearchParams();
        params.set('username', user.email);
        params.set('password', user.password);
        params.set('grant_type', 'password');

        return this.http.post(
          Config.apiUrl + "/o/token/",
          params.toString(),
          { headers: headers }
        )
        .map(response => response.json())
        .do(data => {
          Config.token = data.access_token;
        })
        .catch(this.handleErrors);
      }
    */
    public loginUser(user: User) {
        let params = new URLSearchParams();
        params.set('username', user.email);
        params.set('password', user.password);
        params.set('grant_type', 'password');
        return fetch(Config.apiUrl + "/o/token/?" + params.toString(), {
            method: 'post',
            headers: {
                "Authorization": Config.authHeader,
                "Content-Type": 'application/x-www-form-urlencoded'
            },
        })
            .then(res => {
                if (res.status == 200) {
                    return res.json().then((data) => {
                        Config.token = data.access_token
                    });
                }
                else if (res.status == 401) {
                    return res.json().then((data) => {
                        throw new Error('Error login user...' + ' ' + data.error_description + ' (' + res.status + ')');
                    })
                }
                else {
                    throw new Error('Error login user...');
                }
            });
    }

    /*
      handleErrors(error: Response) {
        console.log(JSON.stringify(error));
        return Observable.throw(error.json().error_description);
      }
      */
}