import { RouterExtensions } from "nativescript-angular/router";
import { Router } from '@angular/router';
import { Injectable } from "@angular/core";

@Injectable()
export class RouterService {

    public isLoading: boolean;

    constructor(private routerExtensions: RouterExtensions, private router: Router) {}

    public simpleForward(url: string) {
        return this.router.navigate([url]).then(() => { return this.pseudoLoading() });
    }

    public simpleBackward(url: string) {
        return this.router.navigate([url]).then(() => { return this.pseudoLoading() });
    }

    public simple(url: string) {
        return this.router.navigate([url]).then(() => { return this.pseudoLoading() });
    }

    private pseudoLoading() {
        //this.isLoading = false
        return setTimeout(() => this.isLoading = false, 500)
    }
}