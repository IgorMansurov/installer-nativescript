import { Base64 } from "js-base64";

class Client {
  static API_CLIENT_ID = 'QFEUAqy9hL0vEOQBkL3mN3u5cezjZfirrSEaLhLH';
  static API_SECRET = 'JQY5lLegTgecFBdNv2iCaK41Pv6ArNfUFTjd4WkeVBpt7XRk2Ra9J8BHKkSfi5AkNLMB4QvvMrpjc0kaQHy5pbkjotse9PoRkjzuKw5CQb2jo5NNKTbCaRJ9EvuB3HTl';
}

export class Config {
  static apiUrl = "https://api.crowcloud.xyz";
  static token = "";
  static authHeader = 'Basic ' + Base64.encode(Client.API_CLIENT_ID + ':' + Client.API_SECRET);
  static XCrowPinstaller = '';

  static pageNumber = 1;
  static pageSize = 20;


  public static channelTypes = {1: 'TCP/IP', 2: 'WiFi', 4: 'GPRS', 8: 'SMS'};
  public static outputTypes = {1: 'Constant', 2: 'Single puls', 4: 'Flash'};
  public static chimeResetModes = {1: 'Chime alarm reset by signal', 2: 'Chime alarm reset by time', 4: 'Chime alarm reset by retrigger Time'};
  public static reportProtocolTypes = {0: 'Crow', 1: 'WEP', 2: 'SIA-09 (SIA-DSC)'};
  public static securityTypes = {0: 'Open', 1: 'WEP', 2: 'WPA/WPA2-PSK', 3: 'Push-button WPS security', 4: 'Pin-based WPS security', 5: 'TI-WPS'};
  public static dateFormats = {1: 'European DD.MM.YYYY', 2: 'American MM.DD.YYYY'};
  public static langs = ['en', 'he', 'ru'];
  public static reservedChannelId: number = 7;

  public static routerAnimationLoginForward = {name: "flipLeft", duration: 0, curve: "linear"};
  public static routerAnimationLoginBackward = {name: "flipRight", duration: 0, curve: "linear"};
  public static routerAnimationCommonForward = {name: "slideLeft", duration: 0, curve: "linear"};
  public static routerAnimationCommonBackward = {name: "slideRight", duration: 0, curve: "linear"};
  public static routerAnimationDelay = 2000;

}