export class Panel {
  public names: string;
  public subNames: string;


  constructor(
      public id: string,
      public mac: string,
      public name: string,
      public control_panel_users: Array<any>,
      public created: string,
      public geo_ip_info: string,
      public group: string,
      public host: string,
      public last_keep_alive: string,
      public latitude: string,
      public longitude: string,
      public pair_type: string,
      public remote_access_password: string,
      public state: string,
      public state_full: string,
      public version: string,
      public group_info: any,
  ) {
      this.setPanelNames();
  }

  public setPanelNames() {
    let names = [];
    for(let userId in this.control_panel_users) {
      names.push(this.control_panel_users[userId]['name']);
    }
    if(names.length > 1) {
      this.names = names[0];
      this.subNames = names.slice(1, names.length).join(', ');
    }
    else if (names.length > 0) {
      this.names = names[0];
      this.subNames = '';
    }
    else {
      this.names = '';
      this.subNames = '';
    }

  }
}