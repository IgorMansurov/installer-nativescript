import {Injectable} from "@angular/core";
import {PanelConfigService} from "./panel-config.service";
import {Helper} from "../../shared/helper";

@Injectable()
export class PanelUsersService {

    constructor(private panelConfigService: PanelConfigService) {
    }

    public getPanelUserList(panelId: number) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        let users = new Array;
        if(panelConfig) {
            panelConfig.user_code.map((element, index, array) => {
                !Helper.isEmpty(element) ? users.push(this.getUserIdAndNameByIndex(panelId, index)) : '';
            });
        }
        return users;
    }

    public quantityEmptyUserCodes(panelId: number) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        if(panelConfig) {
            let emptyCodes = panelConfig.user_code.filter((element, index, array) => {
                return Helper.isEmpty(element);
            });
            return emptyCodes.length;
        }
        else {
            return 0;
        }

    }

    public getFirstEmptyUser(panelId: number) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        if(panelConfig) {
            return this.panelConfigService.getConfig(panelId).user_code.findIndex((element, index, array) => {
                return Helper.isEmpty(element);
            });
        }
        else {
            return null;
        }
    }

    private getUserIdAndNameByIndex(panelId: number, index: number) {
        return {
            id: index,
            user_name: this.panelConfigService.getConfig(panelId).user_name[index]
        };
    }
}