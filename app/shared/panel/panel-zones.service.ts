import {Injectable} from "@angular/core";
import {PanelConfigService} from "./panel-config.service";
import {Helper} from "./../../shared/helper";

@Injectable()
export class PanelZonesService {

    constructor(private panelConfigService: PanelConfigService) {
    }

    public getZoneList(panelId: number) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        let zones = new Array;
        if(panelConfig) {
            panelConfig.radio_zones_list.map((element, index, array) => {
                !Helper.isEmpty(element) ? zones.push({id: index, zone_name: this.panelConfigService.getConfig(panelId).zone_name[index]}) : '';
            });
        }
        return zones;
    }

    public quantityEmptyZones(panelId: number) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        if(panelConfig) {
            let emptyZones = panelConfig.radio_zones_list.filter((element, index, array) => {
                return Helper.isEmpty(element);
            });
            return emptyZones.length;
        }
        else {
            return 0;
        }
    }

    public getFirstEmptyZone(panelId: number) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        if(panelConfig) {
            return this.panelConfigService.getConfig(panelId).radio_zones_list.findIndex((element, index, array) => {
                return Helper.isEmpty(element);
            });
        }
        else {
            return null;
        }
    }
}