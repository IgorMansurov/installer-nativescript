import {Injectable} from "@angular/core";
import {PanelConfigService} from "./panel-config.service";

@Injectable()
export class PanelAreasService {

    constructor(private panelConfigService: PanelConfigService) {
    }

    public getPanelAreaList(panelId: number) {
        let outputs = new Array;
        this.panelConfigService.getConfig(panelId).area_name.map((element, index, array) => {
            outputs.push({id: index, area_name: this.panelConfigService.getConfig(panelId).area_name[index]});
        });
        return outputs;
    }
}