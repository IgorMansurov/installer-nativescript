import { Injectable } from "@angular/core";
import { Config } from "../config";
import * as dialogs from "ui/dialogs";
import {Router} from "@angular/router";

@Injectable()
export class PanelConfigService {

    private panelId: number;
    private panelConfig: any;
    private installerCode: any;

    constructor(private router: Router) {
        this.panelId = null;
        this.panelConfig = null;
        this.installerCode = null;
    }
    public initConfig(panelId: number) {

            this.panelId = panelId;
            return this.getInstallerCode()
                .then(code => {
                      return this.changeConfigMode(this.panelId, code, true)
                        .then(() => {return this.exchangeConfig(this.panelId, code, true)})
                        .then((config)=> {this.panelConfig = config; return this.changeConfigMode(this.panelId, code, false)})
                        .catch((error) => {throw new Error('Error init config: ' + error)})
                });

    }
    public getConfig(panelId: number) {
        if(this.panelId == panelId) {
            return this.panelConfig;
        }
        else {
            this.panelId = null;
            this.panelConfig = null;
            alert("Something went wrong.. Try again!");
            this.router.navigate(['/list']);
        }
    }
    public saveConfig() {
        return this.getInstallerCode()
            .then(code => {
                  return this.changeConfigMode(this.panelId, code, true)
                    .then(() => {return this.exchangeConfig(this.panelId, code, false)})
                    .then((config)=> {this.panelConfig = config; return this.changeConfigMode(this.panelId, code, false)})
                    .catch((error) => {new Error('Error save config: ' + error)})
            });
    }
    public saveConfigLocally(config: any) {
        this.panelConfig = config;
    }

    private isSetCongig(panelId: number) {
        return (this.installerCode && this.panelId == panelId && this.panelConfig) ? true : false;
    }
    /*
    private getInstallerCode() {
        if(!this.installerCode) {
            return dialogs.prompt({
                title: "Installer code",
                message: "",
                okButtonText: "Submit",
                defaultText: "",
                inputType: dialogs.inputType.password
            }).then(res => {
                if(res.result) {
                    this.installerCode = res.text;
                    return res.text;
                }
                else {
                    throw new Error('Installer code not given..');
                }
            });
        }
        else {
            return Promise.resolve(this.installerCode);
        }
    }
    */
    public setInstallerCode(code: number) {
        this.installerCode = code;
    }
    private getInstallerCode() {
        if(!this.installerCode) {
            return Promise.reject('Installer code not set..');
        }
        else {
            return Promise.resolve(this.installerCode);
        }
    }
    public destroyConfig() {
        this.installerCode = null;
    }
    private changeConfigMode(panelId: number, installerCode: string, set = true) {
        let method = set ? 'post' : 'delete';
        return fetch(Config.apiUrl + "/panels/" + panelId + '/config_mode/',{
            method:method,
            headers:{
                "Authorization": "Bearer " + Config.token,
                "X-Crow-CP-installer": installerCode
            }
        })
        .then(res => {
            if(res.status != 200) {
                delete this.installerCode;
                throw new Error('Error change config mode. Response status ' + res.status)
            }

        })
        .catch(() => {this.installerCode = null; throw new Error('Error change config with method ' + method)})
    }
    private exchangeConfig(panelId: number, installerCode: string, recieve = true) {
        let method = recieve ? 'get' : 'put';
        let receive = recieve;
        return fetch(Config.apiUrl + "/panels/" + panelId + '/config_export_import/',{
            method:method,
            headers:{
                "Authorization": "Bearer " + Config.token,
                "Content-Type": "application/json",
                "X-Crow-CP-installer": installerCode
            },
            body: !receive ? JSON.stringify(this.panelConfig) : ''
        })
        .then(res => {
            if(res.status != 200) {
                this.installerCode = null;
                throw new Error('Error exchange config. Response status ' + res.status);
            }
            else {
                return res.json().then(data => data);
            }
        })
        .catch(() => {this.installerCode = null; throw new Error('Error exchange config with method ' + method)})
    }
    public getDiagnosticConfig(panelId: number, communicationType: string) {
        return this.getInstallerCode()
            .then(code => {
                return this.getDiagnosticInfo(this.panelId, code, communicationType);
            })
            .catch(() => {throw new Error('Error get diagnostic config..')});
    }
    private getDiagnosticInfo(panelId: number, installerCode: string, communicationType: string) {
        return fetch(Config.apiUrl + "/panels/" + panelId + '/module/' + communicationType + '/info/',{
            method:'get',
            headers:{
                "Authorization": "Bearer " + Config.token,
                "Content-Type": "application/json",
                "X-Crow-CP-installer": installerCode
            },
        })
        .then(res => {
            if(res.status != 200) {
                this.installerCode = null;
                throw new Error('Error get ' + communicationType +' info. Response status ' + res.status);
            }
            else {
                return res.json().then(data => data);
            }
        })
        .catch(() => {this.installerCode = null; throw new Error('Error get ' + communicationType +' info.')})
    }
    private getHeaders() {
        return {
                "Authorization": "Bearer " + Config.token,
                "Content-Type": "application/json",
                "X-Crow-CP-installer": this.getInstallerCode()
            }
    }
}