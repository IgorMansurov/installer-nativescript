import {Injectable} from "@angular/core";
import {PanelConfigService} from "./panel-config.service";
import {Helper} from "./../../shared/helper";

@Injectable()
export class PanelOutputsService {

    constructor(private panelConfigService: PanelConfigService) {
    }

    public getPanelOutputList(panelId: number) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        let outputs = new Array;
        if(panelConfig) {
            panelConfig.radio_outputs_list.map((element, index, array) => {
                !Helper.isEmpty(element) ? outputs.push(this.getOutputIdAndNameByIndex(panelId, index)) : '';
            });
        }
        return outputs;
    }

    public quantityEmptyOutputs(panelId: number) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        if(panelConfig) {
            let emptyOutputs = panelConfig.radio_outputs_list.filter((element, index, array) => {
                return Helper.isEmpty(element);
            });
            return emptyOutputs.length;
        }
        else {
            return 0;
        }
    }

    public getFirstEmptyOutput(panelId: number) {
        let panelConfig = this.panelConfigService.getConfig(panelId);
        if(panelConfig) {
            return this.panelConfigService.getConfig(panelId).radio_outputs_list.findIndex((element, index, array) => {
                return Helper.isEmpty(element);
            });
        }
        else {
            return null;
        }
    }

    private getOutputIdAndNameByIndex(panelId: number, index: number) {
        return {
            id: index,
            output_name: this.panelConfigService.getConfig(panelId).output_name[index]
        };
    }
}