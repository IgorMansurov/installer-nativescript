import { Injectable } from "@angular/core";
import { Http, Headers, Response, URLSearchParams } from "@angular/http";
import { Observable } from "rxjs/Rx";
import "rxjs/add/operator/map";

import { Config } from "../config";
import { Panel } from "./panel";

@Injectable()
export class PanelListService {
  constructor(private http: Http) {}

  load(params: any) {

    let headers = new Headers();
    headers.append("Authorization", "Bearer " + Config.token);

    let urlParams = new URLSearchParams();
    urlParams.set('page', params.pageNumber);
    urlParams.set('page_size', params.pageSize);
    urlParams.set('enriched', params.enriched);
    urlParams.set('search', params.search);

    return this.http.get(Config.apiUrl + "/management/panels/?" + urlParams.toString(), {
      headers: headers
    })
    .map(res => res.json())
    .map(data => {
      let panelList = [];
      data.results.forEach((panel) => {
        panelList.push(
            new Panel(
                  panel.id,
                  panel.mac,
                  panel.name,
                  panel.control_panel_users,
                  panel.created,
                  panel.geo_ip_info,
                  panel.group,
                  panel.host,
                  panel.last_keep_alive,
                  panel.latitude,
                  panel.longitude,
                  panel.pair_type,
                  panel.remote_access_password,
                  panel.state,
                  panel.state_full,
                  panel.version,
                  panel.group_info
            ));
      });
      return panelList;
    })
    .catch(this.handleErrors);
  }

  handleErrors(error: Response) {
    console.log('error !!!');
    console.log(JSON.stringify(error.json()));
    return Observable.throw(error);
  }

}

