import { platformNativeScriptDynamic } from "nativescript-angular/platform";

import { AppModule } from "./app.module";

import { labelLineHeight } from "./utils/status-bar-util";

//setStatusBarColors();
//labelLineHeight();

platformNativeScriptDynamic().bootstrapModule(AppModule);
